PrefabFiles =
{



	---------------新物品
	"loot_pumper",
	"sea2land_fork",


	-----------修改暴力覆盖
	"wave_shimmer_tropical",
	"wave_shore_tropical",


	-------------------------------

	"house_wall",
	"sparkle_fx",
	"fx",
	"Axes",
	"armor_obsidian",
	"meteor_impact",
	"dug_cofeecactus",
	"turfesvolcanobiome",
	"snake",
	"snake_hole",
	"snakeskin",
	"poisonbubble",
	"venom_gland",
	"harpoon",
	"new_hats",
	"parrot_pirate",
	"dubloon",
	"machetes",
	"tunacan",
	"seagull",
	"toucan",
	"lake",
	--"lakeinside",
	"boatcork",
	"frogpoison",
	"mosquitopoison",
	"cormorant",
	"antidote",
	"chiminea",
	"chimineafire",
	"armor_seashell",
	"ox_horn",
	"ox",
	"oxherd",
	"knightboat_cannonshot",
	"rowboat_wake", --trail do rowboat
	"splash_water",
	"blubber",
	"fish_med",
	"debris", --debris_1, debris_2, debris_3, debris_4 espalhar na praia
	"boatrepairkit",
	"sandbag",
	"cutlass",
	"cookpotfoodssw",
	"luggarechestspawn",
	"porto",
	"porto2",
	"buriedtreasure",
	"windtrail",
	"windswirl",
	"ventania",
	"sail",
	"boattorch",
	"boatlantern",
	"boatcannon",
	"woodlegs_boatcannon",
	"quackeringram",
	"quackenbeak",
	"quackendrill",
	"quackering_wave",
	"quackering_wake",
	"trawlnet",
	"boatsurf",
	"boatsurfothers",
	"boatpirate",
	"turbine_blades",
	"buoy",
	"armor_lifejacket",
	"saplingnova",
	"edgefog",
	"boatpirateamigo",
	"shadowwaxwell_boat",
	"luckyhat",
	"telescope",
	"thatchpack",
	"parrot",
	"seagullwater",
	"seaweed_stalk",
	"corallarve",
	"nubbin",
	"coconade",
	"piratepack",
	"ox_flute",
	"blubbersuit",
	"tarsuit",
	"tarlamp",
	"waterchest",
	"mussel_bed",
	"mussel_stick",
	"messagebottle1",
	"bottlelantern",
	"researchlab5",
	"roe",
	"roe_fish",
	"fishfarm",
	"fishfarm_sign",
	"mussel_farm",
	"sea_chiminea",
	"tar_extractor",
	"seatrap",
	--"flood",
	"hail",
	"wave_ripple",
	-------------------------complemento---------------------------------
	"twister_defogo",
	"twister_tornadodefogo",
	"firetwister_spawner",
	"fire_twister_seal",
	"walani",
	"wilbur",
	"woodlegs",
	"glass",
	"grass_tall",
	"asparagus",
	"deep_jungle_fern_noise",
	"vampirebatcave",
	"vampirebatcave_interior",
	"roc_cave_entrance",
	"roc_cave_interior",
	"vampitecave_deco",
	"spearlauncher",
	"spear_wathgrithr",
	"speargun",
	"jungletreeguard",
	"seataro_planted",
	"seatarospawner",
	"ox_wool",
	"conch",
	"seacucumber",
	"watercress",
	"jungletreeguard_snake",
	"armor_cactus",
	"scorpionte",
	"cookpotfoodshamlet",
	"cork",
	"corkbat",
	"hats_hamlet",
	"sedimentpuddle",
	"flood_ice",
	"snake2",
	"bramble",
	"bramble_bulb",
	"dungball",
	"dungbeetle",
	"dungpile",
	"chitin",
	"pinkman",
	"oincpile",
	"piggolem",
	"turfshamlet",
	"krakenchest",
	"lavaarena_hound",
	"lavaarena_knight",
	"lavaarena_merm",
	"lavaarena_spider",
	"lavaarena_bishop",
	"frog_poison",
	"spearsw",
	"boatmetal",
	"bird_swarm",
	"birds_ham",
	"blunderbuss",

	"cloudpuff",
	"bishopwater",
	"rookwater",
	"solofish",
	"tropicalspawnblocker",
	"whaletrack",
	"swfishbait",

	"beds",
	"rug",
	"shears",
	"gold_dust",
	"goldpan",
	-------
	"icebearger", -------这是永霜岛的
	"icedeerclops",
	"icerockcreatures",
	"cave_exit_vulcao",
	"cave_entrance_vulcao",
	"vidanomar",
	"vidanomarseaworld",
	"ashfx",
	"boat_raft_rot",
	-- "panda", -----------这是海难plus的
	-- "pandaskin",
	-- "pandatree",
	"nectar_pod",
	"piggravestone",
	"mangrovespawner", --------刷新点
	"oxwaterspawner",
	"grasswaterspawner",
	"waterreedspawner",
	"fishinholewaterspawner",
	"poisonbalm",
	"seasack",
	"magic_seal",
	"wind_conch",
	"sail_stick",
	"armor_windbreaker",
	"vine",
	"basefan",
	"floodsw",
	"birdwhistle", -----------鸟笛
	"bundled_structure",
	"lavaarena_armor",
	"lavarenainside",
	"teleportato2",
	"galinheiro",
	"invisiblepondfish",
	"spider_mutators_new",
	"anthillcave",
	"anthill_cavelamp",
	"grotto_grub_nest",
	"grotto_grub",
	"grotto_parsnip",

	"grottoqueen",
	-- "otter",
	-- "icedpad",
	--"yeti",
	-- "artic_flower",
	"watertree_pillar2",
	"firerain",
	"lavapool",
	"obsidian",
	"bramble_bush",
	"tidalpoolnew",
	"marsh_tree_new",
	-- "ligamundo",
}

-- if GetModConfigData("whirlpools") then
table.insert(PrefabFiles, "whirlpool")
-- end

table.insert(PrefabFiles, "pigbanditexit")
-- if GetModConfigData("tropicalshards") ~= 0 then
table.insert(PrefabFiles, "porkland_sw_entrance")
-- end

-- if GetModConfigData("kindofworld") == 10 or GetModConfigData("raftlog") then
table.insert(PrefabFiles, "boatraft_old")
table.insert(PrefabFiles, "boatlog_old")
-- end
table.insert(PrefabFiles, "boatraft") -------------这个是联机版的船？

table.insert(PrefabFiles, "vampirebat")
table.insert(PrefabFiles, "aporkalypse_clock")
table.insert(PrefabFiles, "hanging_vine")
table.insert(PrefabFiles, "grabbing_vine")

table.insert(PrefabFiles, "dragoonegg") --jogo da erro de memoria se tirar
table.insert(PrefabFiles, "tigersharkpool")
table.insert(PrefabFiles, "tigersharktorch")
table.insert(PrefabFiles, "hatty_piggy_tfc")
table.insert(PrefabFiles, "boarmound")
table.insert(PrefabFiles, "roc_nest")
table.insert(PrefabFiles, "glowfly")
table.insert(PrefabFiles, "rabid_beetle")
table.insert(PrefabFiles, "obsidianstaff")
table.insert(PrefabFiles, "mermhouse_fisher")
table.insert(PrefabFiles, "mermtrader")
table.insert(PrefabFiles, "pig_scepter")


table.insert(PrefabFiles, "quagmire_mushroomstump")
table.insert(PrefabFiles, "quagmire_mushrooms")
table.insert(PrefabFiles, "quagmire_fern")
table.insert(PrefabFiles, "quagmire_pebblecrab")
table.insert(PrefabFiles, "quagmire_safe")
table.insert(PrefabFiles, "wildbeaver")
table.insert(PrefabFiles, "wildbeaver_house")
table.insert(PrefabFiles, "beaverskin")
table.insert(PrefabFiles, "quagmire_park_gate")
table.insert(PrefabFiles, "quagmire_parkspike")
table.insert(PrefabFiles, "wildbeaverguard")
table.insert(PrefabFiles, "beaver_head")
table.insert(PrefabFiles, "beavertorch")
table.insert(PrefabFiles, "quagmire_key")
table.insert(PrefabFiles, "quagmire_goatkid")


-- if GetModConfigData("kindofworld") == 5 or GetModConfigData("enableallprefabs") == true then
table.insert(PrefabFiles, "roc")
table.insert(PrefabFiles, "roc_leg")
table.insert(PrefabFiles, "roc_head")
table.insert(PrefabFiles, "roc_tail")
-- end
table.insert(PrefabFiles, "roc_robin_egg")
table.insert(PrefabFiles, "ro_bin_gizzard_stone")
table.insert(PrefabFiles, "ro_bin")
table.insert(PrefabFiles, "gnatmound")
table.insert(PrefabFiles, "gnat")

---------------lillypad biome------------------------
-- if GetModConfigData("lilypad") ~= 0 or GetModConfigData("enableallprefabs") == true or GetModConfigData("kindofworld") == 5 or GetModConfigData("hamletcaves_shipwreckedworld") == 1 then
table.insert(PrefabFiles, "hippo_antler")
table.insert(PrefabFiles, "hippoherd")
table.insert(PrefabFiles, "hippoptamoose")
table.insert(PrefabFiles, "lillypad")
table.insert(PrefabFiles, "lotus")
table.insert(PrefabFiles, "lotus_flower1")
table.insert(PrefabFiles, "bill")
table.insert(PrefabFiles, "bill_quill")
table.insert(PrefabFiles, "reeds_water")
-- end
---------------lavaarena volcano---------------------
-- table.insert(PrefabFiles, "flame_elemental_tfc")
-- table.insert(PrefabFiles, "tfwp_elemental")
-- table.insert(PrefabFiles, "tfwp_hats")
-- table.insert(PrefabFiles, "tfwp_armor")
-- -- table.insert(PrefabFiles,"tfwp_control_book")
-- -- table.insert(PrefabFiles,"tfwp_summon_book")
-- table.insert(PrefabFiles, "tfwp_lava_hammer")
-- table.insert(PrefabFiles, "tfwp_spear_gung")
-- table.insert(PrefabFiles, "tfwp_spear_lance")
-- table.insert(PrefabFiles, "tfwp_healing_staff")
-- table.insert(PrefabFiles, "tfwp_infernal_staff")
-- table.insert(PrefabFiles, "tfwp_dragon_dart")
-- table.insert(PrefabFiles, "tfwp_lava_dart")
-- --table.insert(PrefabFiles,"tfwp_freeze_emitter")
-- table.insert(PrefabFiles, "tfwp_fire_bomb")
-- table.insert(PrefabFiles, "tfwp_heavy_sword")

-- table.insert(PrefabFiles, "tfwp_riledlucy")
-- table.insert(PrefabFiles, "tfwp_forge_books")
-- table.insert(PrefabFiles, "tfwp_forge_fireball_projectile")
-- table.insert(PrefabFiles, "tfwp_infernalstaff_meteor")
-- table.insert(PrefabFiles, "tfwp_weaponsparks_fx")
-- table.insert(PrefabFiles, "tfwp_forgespear_fx")
-- table.insert(PrefabFiles, "tfwp_healingcircle")
-- table.insert(PrefabFiles, "tfwp_healingcircle_regenbuff")

-- -- if GetModConfigData("greenworld") or GetModConfigData("forge") == 1 then
-- table.insert(PrefabFiles, "lavaarena_bossboar")
-- end

-- if GetModConfigData("forge") == 1 then
-- 	table.insert(PrefabFiles, "teleportato_sw_parts")
-- 	table.insert(PrefabFiles, "teleportato_sw")
-- 	table.insert(PrefabFiles, "lavaarena_rhinodrill")
-- 	table.insert(PrefabFiles, "lavaarena_beetletaur")
-- 	table.insert(PrefabFiles, "lavaarena_boarlord")
-- 	table.insert(PrefabFiles, "lavaarena_spectator")
-- 	table.insert(PrefabFiles, "lavaarena_spawner2")
-- 	table.insert(PrefabFiles, "lavaarena_center")
-- 	table.insert(PrefabFiles, "lavaarena_floorgrate")
-- 	table.insert(PrefabFiles, "lavarenaescada")
-- 	table.insert(PrefabFiles, "strange_scorpion_tfc")
-- 	table.insert(PrefabFiles, "lizardman_tfc")
-- 	table.insert(PrefabFiles, "spiky_turtle_tfc")
-- 	table.insert(PrefabFiles, "spiky_monkey_tfc")
-- 	table.insert(PrefabFiles, "lavarenawaves")
-- end

-- table.insert(PrefabFiles, "quagmire_coins")

--------------------------------------------------------
table.insert(PrefabFiles, "obsidianbomb")
table.insert(PrefabFiles, "obsidianbombactive")
table.insert(PrefabFiles, "fabric")
table.insert(PrefabFiles, "armor_snakeskin")
-- if GetModConfigData("Shipwrecked") ~= 5 and GetModConfigData("kindofworld") ~= 5 or GetModConfigData("enableallprefabs") == true or GetModConfigData("Shipwrecked_plus") == true or GetModConfigData("kindofworld") == 20 then
table.insert(PrefabFiles, "crate")
table.insert(PrefabFiles, "dragoon")
table.insert(PrefabFiles, "dragoonfire")
table.insert(PrefabFiles, "dragoonspit")
table.insert(PrefabFiles, "dragoonden")
table.insert(PrefabFiles, "dragoonheart")
table.insert(PrefabFiles, "volcano_shrub")
table.insert(PrefabFiles, "flamegeyser")
table.insert(PrefabFiles, "rock_obsidian")
table.insert(PrefabFiles, "magma_rocks")
table.insert(PrefabFiles, "obsidian_workbench")
table.insert(PrefabFiles, "obsidianfirepit")
table.insert(PrefabFiles, "obsidianfirefire")
table.insert(PrefabFiles, "elephantcactus")
table.insert(PrefabFiles, "coffeebush")
table.insert(PrefabFiles, "coffeebeans")
table.insert(PrefabFiles, "coffee")
table.insert(PrefabFiles, "lavaerupt")
table.insert(PrefabFiles, "volcano")
table.insert(PrefabFiles, "woodlegs_cage")
table.insert(PrefabFiles, "woodlegs_key1")
table.insert(PrefabFiles, "woodlegs_key2")
table.insert(PrefabFiles, "woodlegs_key3")
table.insert(PrefabFiles, "woodlegs1")
table.insert(PrefabFiles, "woodlegsghost")
table.insert(PrefabFiles, "woodlegs_unlock")
table.insert(PrefabFiles, "vulcano")
table.insert(PrefabFiles, "escadadovulcao")
--table.insert(PrefabFiles,"mermfisher")
table.insert(PrefabFiles, "flup")
table.insert(PrefabFiles, "flupegg")
table.insert(PrefabFiles, "tidalpool")
table.insert(PrefabFiles, "poisonhole")
table.insert(PrefabFiles, "tigershark")
table.insert(PrefabFiles, "tigersharkshadow")
table.insert(PrefabFiles, "volcanofog")
table.insert(PrefabFiles, "flupspawner")
table.insert(PrefabFiles, "jungletrees")
table.insert(PrefabFiles, "jungletreeseed")
table.insert(PrefabFiles, "bambootree")
table.insert(PrefabFiles, "bamboo")
table.insert(PrefabFiles, "wildbore")
table.insert(PrefabFiles, "wildborehouse")
table.insert(PrefabFiles, "doydoy")
table.insert(PrefabFiles, "doydoy_mating_fx")
table.insert(PrefabFiles, "doydoyegg")
table.insert(PrefabFiles, "doydoyfeather")
table.insert(PrefabFiles, "doydoyherd")
table.insert(PrefabFiles, "doydoynest")
table.insert(PrefabFiles, "livingjungletree")
table.insert(PrefabFiles, "doydoy_spawner")
table.insert(PrefabFiles, "berrybush2_snake")
table.insert(PrefabFiles, "lavapondbig")
table.insert(PrefabFiles, "bigfoot")
table.insert(PrefabFiles, "glommerbell")
table.insert(PrefabFiles, "sweet_potato")
table.insert(PrefabFiles, "doydoyfan")
table.insert(PrefabFiles, "sand_castle")
table.insert(PrefabFiles, "sandhill")
table.insert(PrefabFiles, "sand")
table.insert(PrefabFiles, "seashell")
table.insert(PrefabFiles, "seashell_beached")
table.insert(PrefabFiles, "rock_limpet")
table.insert(PrefabFiles, "limpets")
table.insert(PrefabFiles, "palmleaf")
table.insert(PrefabFiles, "palmleafhut")
table.insert(PrefabFiles, "palmtrees")
table.insert(PrefabFiles, "coconut")
table.insert(PrefabFiles, "crab")
table.insert(PrefabFiles, "crabhole")
table.insert(PrefabFiles, "treeguard")
table.insert(PrefabFiles, "treeguard_coconut")
table.insert(PrefabFiles, "warningshadow")
table.insert(PrefabFiles, "slotmachine")
table.insert(PrefabFiles, "sharkitten")
table.insert(PrefabFiles, "sharkittenspawner")
table.insert(PrefabFiles, "bush_vine")
table.insert(PrefabFiles, "primeapebarrel")
table.insert(PrefabFiles, "monkeyball")
table.insert(PrefabFiles, "shark_gills")
table.insert(PrefabFiles, "primeape")
table.insert(PrefabFiles, "icemaker")
table.insert(PrefabFiles, "tigereye")
table.insert(PrefabFiles, "packim")
table.insert(PrefabFiles, "packim_fishbone")
-- end

-------- pprefabs gorge -----------

-- table.insert(PrefabFiles, "quagmire_portal_key")
-- table.insert(PrefabFiles, "q_swampig")
-- table.insert(PrefabFiles, "q_swampig_house")
-- table.insert(PrefabFiles, "q_pond")
-- table.insert(PrefabFiles, "q_beefalo")
-- table.insert(PrefabFiles, "q_sugarwoodtree")
-- table.insert(PrefabFiles, "q_sap")
-- table.insert(PrefabFiles, "q_sugarwoodtree_sapling")
-- table.insert(PrefabFiles, "quagmiregoat")
-- table.insert(PrefabFiles, "quagmiregoatherd")
-- table.insert(PrefabFiles, "q_sugarwoodtree_cone")
-- table.insert(PrefabFiles, "q_pigeon")
-- table.insert(PrefabFiles, "q_spiceshrub")
-- table.insert(PrefabFiles, "maxwellendgame")
-- table.insert(PrefabFiles, "maxwelllight")
-- table.insert(PrefabFiles, "maxwelllight_flame")
-- table.insert(PrefabFiles, "maxwellminions")
-- table.insert(PrefabFiles, "maxwellboss")
-- table.insert(PrefabFiles, "maxwelllock")
-- table.insert(PrefabFiles, "maxwellshadowmeteor")
-- table.insert(PrefabFiles, "maxwellphonograph")
-- table.insert(PrefabFiles, "maxwellestatua")
-- table.insert(PrefabFiles, "maxwellshadowheart")

table.insert(PrefabFiles, "tree_forest")
table.insert(PrefabFiles, "tree_forest_deep")
table.insert(PrefabFiles, "tree_forest_rot")
table.insert(PrefabFiles, "tree_forestseed")
table.insert(PrefabFiles, "spider_monkey_tree")
table.insert(PrefabFiles, "spider_monkey")
table.insert(PrefabFiles, "spider_monkey_herd")
table.insert(PrefabFiles, "spider_ape")
table.insert(PrefabFiles, "trapslug")
table.insert(PrefabFiles, "antman2")
table.insert(PrefabFiles, "fennel")
table.insert(PrefabFiles, "pig_palace2")
table.insert(PrefabFiles, "pig_palace2_interior")
table.insert(PrefabFiles, "peagawk_prism")
table.insert(PrefabFiles, "peagawkfeather_prism")
table.insert(PrefabFiles, "city_lamp2")
table.insert(PrefabFiles, "pig_guard_tower2")
table.insert(PrefabFiles, "wall_spawn_city")
table.insert(PrefabFiles, "spider_ape_tree")
table.insert(PrefabFiles, "slipstor")
table.insert(PrefabFiles, "slip")
table.insert(PrefabFiles, "slipstor_spawner")

-- if GetModConfigData("Shipwrecked_plus") == true or GetModConfigData("enableallprefabs") == true or GetModConfigData("Shipwreckedworld_plus") == true then
-- 	table.insert(PrefabFiles, "goldbishop")
-- 	table.insert(PrefabFiles, "goldentomb")
-- 	table.insert(PrefabFiles, "goldmonkey")
-- 	table.insert(PrefabFiles, "goldbishop")
-- 	table.insert(PrefabFiles, "goldobi")


-- 	table.insert(PrefabFiles, "tikihead")
-- 	table.insert(PrefabFiles, "teepee")
-- 	table.insert(PrefabFiles, "tikimask")
-- 	table.insert(PrefabFiles, "tikifire")
-- 	table.insert(PrefabFiles, "wanawanatiki")
-- 	table.insert(PrefabFiles, "tikistick")

-- 	table.insert(PrefabFiles, "summerwalrus")
-- 	table.insert(PrefabFiles, "summerigloo")

-- 	table.insert(PrefabFiles, "octoatt")
-- 	table.insert(PrefabFiles, "octopus")
-- 	table.insert(PrefabFiles, "octohouse")
-- end

----- prefabs marinhos---------------
table.insert(PrefabFiles, "coralreef")
table.insert(PrefabFiles, "coral")
table.insert(PrefabFiles, "seaweed_planted")
table.insert(PrefabFiles, "seaweed")
table.insert(PrefabFiles, "mangrovetrees")
table.insert(PrefabFiles, "mangrovetreesbee")
table.insert(PrefabFiles, "spidercoralhole")
table.insert(PrefabFiles, "tentacleunderwater")
table.insert(PrefabFiles, "grass_water")
table.insert(PrefabFiles, "rocksunderwater")
table.insert(PrefabFiles, "wreck")

table.insert(PrefabFiles, "fishinhole")
table.insert(PrefabFiles, "octopusking")
table.insert(PrefabFiles, "kraken_tentacle")
table.insert(PrefabFiles, "kraken_projectile")
table.insert(PrefabFiles, "kraken")
table.insert(PrefabFiles, "kraken_jellyfish")
table.insert(PrefabFiles, "kraken_spawner")
table.insert(PrefabFiles, "solofish")
table.insert(PrefabFiles, "swordfish")

table.insert(PrefabFiles, "sharx")
table.insert(PrefabFiles, "stungray")
table.insert(PrefabFiles, "pirateghost")
table.insert(PrefabFiles, "redbarrel")
table.insert(PrefabFiles, "lobsterhole")
table.insert(PrefabFiles, "mussel")
table.insert(PrefabFiles, "waterygrave")
table.insert(PrefabFiles, "bioluminescence")
table.insert(PrefabFiles, "boatrowarmored")
table.insert(PrefabFiles, "boatrowcargo")
table.insert(PrefabFiles, "rawling")
table.insert(PrefabFiles, "coral_brain_rock")
table.insert(PrefabFiles, "coral_brain")
table.insert(PrefabFiles, "limestone")
table.insert(PrefabFiles, "shark_fin")

table.insert(PrefabFiles, "rainbowjellyfish")
table.insert(PrefabFiles, "rainbowjellyfish_planted")
table.insert(PrefabFiles, "sea_yard")
table.insert(PrefabFiles, "sea_yard_arms_fx")
table.insert(PrefabFiles, "tar")
table.insert(PrefabFiles, "tar_pool")
table.insert(PrefabFiles, "bioluminescence_spawner")
table.insert(PrefabFiles, "boatrow")
table.insert(PrefabFiles, "flotsam_debris_sw")
table.insert(PrefabFiles, "boatrowencrusted")
table.insert(PrefabFiles, "ballphin")
table.insert(PrefabFiles, "ballphinhouse")

table.insert(PrefabFiles, "dorsalfin")
table.insert(PrefabFiles, "ballphinpod")
table.insert(PrefabFiles, "jellyfish")
table.insert(PrefabFiles, "jellyfish_planted")
table.insert(PrefabFiles, "crocodog_spawner")
table.insert(PrefabFiles, "crocodog")
table.insert(PrefabFiles, "whale")
table.insert(PrefabFiles, "whale_carcass")
table.insert(PrefabFiles, "knightboat")
table.insert(PrefabFiles, "poisonmistparticle")


table.insert(PrefabFiles, "deco_util")
table.insert(PrefabFiles, "deco_util2")
table.insert(PrefabFiles, "deco_room_window")
-- table.insert(PrefabFiles, "deco_room_ornament")
table.insert(PrefabFiles, "deco_swinging_light")
table.insert(PrefabFiles, "deco_lightglow")
table.insert(PrefabFiles, "pig_shop_spears")

-- if GetModConfigData("pigcity1") ~= 5 or GetModConfigData("pigcity2") ~= 5 or GetModConfigData("kindofworld") == 5 or GetModConfigData("frost_island") ~= 5 or GetModConfigData("enableallprefabs") == true or GetModConfigData("hamletcaves_shipwreckedworld") == 1 then
table.insert(PrefabFiles, "topiary")
table.insert(PrefabFiles, "lawnornaments")
table.insert(PrefabFiles, "hedge")
table.insert(PrefabFiles, "clippings")
table.insert(PrefabFiles, "city_lamp")
table.insert(PrefabFiles, "hamlet_cones")
table.insert(PrefabFiles, "securitycontract")
table.insert(PrefabFiles, "city_hammer")
table.insert(PrefabFiles, "magnifying_glass")
table.insert(PrefabFiles, "pigbandit")
table.insert(PrefabFiles, "banditmap")
table.insert(PrefabFiles, "pig_shop")
table.insert(PrefabFiles, "pig_shop_produce_interior")
table.insert(PrefabFiles, "pig_shop_hoofspa_interior")
table.insert(PrefabFiles, "pig_shop_general_interior")
table.insert(PrefabFiles, "pig_shop_florist_interior")
table.insert(PrefabFiles, "pig_shop_deli_interior")
table.insert(PrefabFiles, "pig_shop_academy_interior")

table.insert(PrefabFiles, "pig_shop_cityhall_interior")
table.insert(PrefabFiles, "pig_shop_cityhall_player_interior")

table.insert(PrefabFiles, "pig_shop_hatshop_interior")
table.insert(PrefabFiles, "pig_shop_weapons_interior")
table.insert(PrefabFiles, "pig_shop_bank_interior")
table.insert(PrefabFiles, "pig_shop_arcane_interior")
table.insert(PrefabFiles, "pig_shop_antiquities_interior")
table.insert(PrefabFiles, "pig_shop_tinker_interior")
table.insert(PrefabFiles, "pig_palace_interior")
table.insert(PrefabFiles, "pig_palace")

table.insert(PrefabFiles, "pigman_shopkeeper_desk")
table.insert(PrefabFiles, "shop_pedestals")
table.insert(PrefabFiles, "deed")
table.insert(PrefabFiles, "playerhouse_city_interior")
-- table.insert(PrefabFiles, "playerhouse_city_interior2")
table.insert(PrefabFiles, "shelf")
table.insert(PrefabFiles, "shelf_slot")
table.insert(PrefabFiles, "trinkets_giftshop")
table.insert(PrefabFiles, "key_to_city")
table.insert(PrefabFiles, "wallpaper")
table.insert(PrefabFiles, "player_house_kits")
table.insert(PrefabFiles, "player_house")

table.insert(PrefabFiles, "pigman_city")
table.insert(PrefabFiles, "pighouse_city")
table.insert(PrefabFiles, "pig_guard_tower")
table.insert(PrefabFiles, "armor_metal")
table.insert(PrefabFiles, "reconstruction_project")
table.insert(PrefabFiles, "water_spray")
table.insert(PrefabFiles, "water_pipe")
table.insert(PrefabFiles, "sprinkler1")
table.insert(PrefabFiles, "alloy")
table.insert(PrefabFiles, "smelter")
table.insert(PrefabFiles, "halberd")
table.insert(PrefabFiles, "oinc")
table.insert(PrefabFiles, "oinc10")
table.insert(PrefabFiles, "oinc100")
-- end

-- if GetModConfigData("startlocation") == 15 or GetModConfigData("kindofworld") == 5 or GetModConfigData("enableallprefabs") == true then
table.insert(PrefabFiles, "porklandintro")
-- end


-- if TUNING.tropical.sealnado or GetModConfigData("enableallprefabs") == true then
table.insert(PrefabFiles, "twister")
table.insert(PrefabFiles, "twister_spawner")
table.insert(PrefabFiles, "twister_seal")
table.insert(PrefabFiles, "twister_tornado")
-- end



-- if GetModConfigData("frost_island") ~= 5 and GetModConfigData("kindofworld") ~= 5 or GetModConfigData("enableallprefabs") == true then
-- table.insert(PrefabFiles, "billsnow")
-- table.insert(PrefabFiles, "giantsnow")
-- table.insert(PrefabFiles, "snowman")
-- table.insert(PrefabFiles, "bear")
-- table.insert(PrefabFiles, "ice_deer")
-- table.insert(PrefabFiles, "mammoth")
-- table.insert(PrefabFiles, "bearden")
-- table.insert(PrefabFiles, "snowitem")
-- table.insert(PrefabFiles, "snow_dune")
-- table.insert(PrefabFiles, "snow_castle")
-- table.insert(PrefabFiles, "cratesnow")
-- table.insert(PrefabFiles, "snowpile1")
-- table.insert(PrefabFiles, "snowbigball")
-- table.insert(PrefabFiles, "snowbeetle")
-- table.insert(PrefabFiles, "snowspider_spike")
-- table.insert(PrefabFiles, "snowspiderden")
-- table.insert(PrefabFiles, "snowberrybush")
-- table.insert(PrefabFiles, "snowspider")
-- table.insert(PrefabFiles, "snowspider2")
-- table.insert(PrefabFiles, "snowspider_spike2")
-- table.insert(PrefabFiles, "snowspiderden2")
-- table.insert(PrefabFiles, "rock_ice_frost")
-- table.insert(PrefabFiles, "icepillar")
-- table.insert(PrefabFiles, "snowgoat")
-- table.insert(PrefabFiles, "snowgoatherd")
-- table.insert(PrefabFiles, "snowwarg")
-- table.insert(PrefabFiles, "snowperd")
-- table.insert(PrefabFiles, "snowdeciduoustrees")
-- table.insert(PrefabFiles, "rock_ice_frost_spawner")
-- table.insert(PrefabFiles, "snowwarg_spawner")
-- end
--[[
if (GetModConfigData("frost_island") == 15 or GetModConfigData("frost_island") == 25) and GetModConfigData("kindofworld") ~= 5 then
table.insert(PrefabFiles,"lavarenainside")
table.insert(PrefabFiles,"teleportato2")
table.insert(PrefabFiles,"telebase")


table.insert(PrefabFiles,"quest")
table.insert(PrefabFiles,"maxwellinside")	
table.insert(PrefabFiles,"maxwellportal")
table.insert(PrefabFiles,"maxwellthrone")

end
]]

table.insert(PrefabFiles, "chicken")
table.insert(PrefabFiles, "peekhen")
table.insert(PrefabFiles, "peekhenspawner")
table.insert(PrefabFiles, "snapdragon")
table.insert(PrefabFiles, "snapdragonherd")
table.insert(PrefabFiles, "crabapple_tree")
table.insert(PrefabFiles, "zeb")
table.insert(PrefabFiles, "wildboreking")
table.insert(PrefabFiles, "wildbore_minion")
table.insert(PrefabFiles, "wildborekingstaff")
table.insert(PrefabFiles, "wildboreking_spawner")


-- if GetModConfigData("Hamlet") ~= 5 or GetModConfigData("startlocation") == 15 or GetModConfigData("kindofworld") == 5 or GetModConfigData("enableallprefabs") == true or GetModConfigData("hamletcaves_shipwreckedworld") == 1 then --GetModConfigData("Plains_Hamlet")
table.insert(PrefabFiles, "rainforesttrees")
table.insert(PrefabFiles, "rainforesttree_sapling")
table.insert(PrefabFiles, "meteor_impact")
table.insert(PrefabFiles, "tuber")
table.insert(PrefabFiles, "tubertrees")
table.insert(PrefabFiles, "pangolden")
table.insert(PrefabFiles, "thunderbird")
table.insert(PrefabFiles, "thunderbirdnest")
table.insert(PrefabFiles, "ancient_robots")
table.insert(PrefabFiles, "ancient_hulk")
table.insert(PrefabFiles, "ancient_herald")
table.insert(PrefabFiles, "armor_vortex_cloak")
table.insert(PrefabFiles, "living_artifact")
table.insert(PrefabFiles, "rock_basalt")
table.insert(PrefabFiles, "ancient_robots_assembly")
table.insert(PrefabFiles, "laser_ring")
table.insert(PrefabFiles, "laser")
table.insert(PrefabFiles, "iron")

table.insert(PrefabFiles, "pheromonestone")

-- if GetModConfigData("luajit") then
table.insert(PrefabFiles, "pig_ruins_maze_old")
-- else
-- if GetModConfigData("compactruins") then
table.insert(PrefabFiles, "pig_ruins_mazecompact")
-- else
table.insert(PrefabFiles, "pig_ruins_maze")
-- end
-- end

table.insert(PrefabFiles, "anthill_interior")
table.insert(PrefabFiles, "anthill_lamp")
table.insert(PrefabFiles, "anthill_stalactite")
table.insert(PrefabFiles, "antchest")
table.insert(PrefabFiles, "antcombhome")
table.insert(PrefabFiles, "antcombhomecave")
table.insert(PrefabFiles, "giantgrub")
table.insert(PrefabFiles, "antqueen")
table.insert(PrefabFiles, "rocksham")
table.insert(PrefabFiles, "deco_ruins_fountain")

table.insert(PrefabFiles, "pig_ruins_entrance_interior")
table.insert(PrefabFiles, "pig_ruins_entrance")
table.insert(PrefabFiles, "pig_ruins_dart_statue")
table.insert(PrefabFiles, "pig_ruins_dart")
table.insert(PrefabFiles, "pig_ruins_creeping_vines")
table.insert(PrefabFiles, "pig_ruins_pressure_plate")
table.insert(PrefabFiles, "pig_ruins_spear_trap")
table.insert(PrefabFiles, "smashingpot")
table.insert(PrefabFiles, "pig_ruins_light_beam")
table.insert(PrefabFiles, "littlehammer")
table.insert(PrefabFiles, "relics")
table.insert(PrefabFiles, "gascloud")
table.insert(PrefabFiles, "bugrepellent")


table.insert(PrefabFiles, "teatree_nut")
table.insert(PrefabFiles, "teatrees")
table.insert(PrefabFiles, "piko")
table.insert(PrefabFiles, "clawpalmtrees")
table.insert(PrefabFiles, "clawpalmtree_sapling")
table.insert(PrefabFiles, "bugfood")
table.insert(PrefabFiles, "rock_flippable")
table.insert(PrefabFiles, "peagawkfeather")
table.insert(PrefabFiles, "peagawk")
table.insert(PrefabFiles, "aloe")
table.insert(PrefabFiles, "pog")
table.insert(PrefabFiles, "weevole")
table.insert(PrefabFiles, "weevole_carapace")
table.insert(PrefabFiles, "armor_weevole")
table.insert(PrefabFiles, "pugalisk")
table.insert(PrefabFiles, "pugalisk_trap_door")
table.insert(PrefabFiles, "pugalisk_ruins_pillar")
table.insert(PrefabFiles, "pugalisk_fountain")
table.insert(PrefabFiles, "pugalisk_fountain_made")
table.insert(PrefabFiles, "waterdrop")
table.insert(PrefabFiles, "floweroflife")
table.insert(PrefabFiles, "gaze_beam")
table.insert(PrefabFiles, "snake_bone")
table.insert(PrefabFiles, "mandrakehouse")
table.insert(PrefabFiles, "mandrakeman")

table.insert(PrefabFiles, "jungle_border_vine")
table.insert(PrefabFiles, "light_rays_ham")
table.insert(PrefabFiles, "nettle")
table.insert(PrefabFiles, "nettle_plant")
table.insert(PrefabFiles, "rainforesttrees")
table.insert(PrefabFiles, "rainforesttree_sapling")
table.insert(PrefabFiles, "tree_pillar")
table.insert(PrefabFiles, "flower_rainforest")
table.insert(PrefabFiles, "pig_ruins_torch")
table.insert(PrefabFiles, "mean_flytrap")
table.insert(PrefabFiles, "radish")
table.insert(PrefabFiles, "adult_flytrap")
table.insert(PrefabFiles, "antman_warrior_egg")
table.insert(PrefabFiles, "antman_warrior")
table.insert(PrefabFiles, "antman")
table.insert(PrefabFiles, "antlarva")
table.insert(PrefabFiles, "anthill")
table.insert(PrefabFiles, "antsuit")
table.insert(PrefabFiles, "venus_stalk")
table.insert(PrefabFiles, "walkingstick")
table.insert(PrefabFiles, "cloudpuff")
-- end

Assets =
{

	----------------------新的材质包-------
	Asset("ATLAS", "images/inventoryimages/loot_pump.xml"),
	Asset("ATLAS", "minimap/loot_pump.xml"),

	Asset("IMAGE", "images/sea2land_fork.tex"),
	Asset("ATLAS", "images/sea2land_fork.xml"),
	Asset("ANIM", "anim/swap_land_fork.zip"),

	------------------从architectpack添加
	-- Asset("IMAGE", "images/inventoryimages/tap_buildingimages.tex"),
	-- Asset("ATLAS", "images/inventoryimages/tap_buildingimages.xml"),

	-- Asset("IMAGE", "images/inventoryimages/tap_buildingimages2.tex"),
	-- Asset("ATLAS", "images/inventoryimages/tap_buildingimages2.xml"),

	-- Asset("IMAGE", "images/minimapimages/tap_minimapicons.tex"),
	-- Asset("ATLAS", "images/minimapimages/tap_minimapicons.xml"),

	--Asset("SOUNDPACKAGE", "sound/volcano.fev"),
	--Asset("SOUND", "sound/volcano.fsb"),
	--Asset("SOUND", "sound/boats.fsb"),
	--Asset("SOUND", "sound/creatures.fsb"),
	--Asset("SOUND", "sound/slot_machine.fsb"),
	--Asset("SOUND", "sound/waves.fsb"),
	--LOD SOUND FILE
	Asset("SOUNDPACKAGE", "sound/dontstarve_DLC002.fev"),
	Asset("SOUNDPACKAGE", "sound/sw_character.fev"),
	Asset("SOUND", "sound/dontstarve_shipwreckedSFX.fsb"),
	Asset("SOUND", "sound/sw_character.fsb"),
	Asset("SOUNDPACKAGE", "sound/dontstarve_DLC003.fev"),
	Asset("SOUND", "sound/DLC003_sfx.fsb"),
	Asset("IMAGE", "images/fog_cloud.tex"),
	--Asset("SOUND", "sound/amb_stream_SW.fsb"),
	--NEW SOUND FILE
	--Asset("SOUNDPACKAGE", "sound/volcano_new.fev"),
	--Asset("SOUND", "sound/volcano_new.fsb"),
	--Asset("SOUNDPACKAGE", "sound/tropical.fev"),
	--Asset("SOUND", "sound/tropical.fsb"),

	Asset("IMAGE", "images/barco.tex"),
	Asset("ATLAS", "images/barco.xml"),

	Asset("ATLAS", "images/inventoryimages/volcanoinventory.xml"),
	Asset("IMAGE", "images/inventoryimages/volcanoinventory.tex"),
	Asset("ATLAS", "images/inventoryimages/novositens.xml"),
	Asset("IMAGE", "images/inventoryimages/novositens.tex"),

	Asset("ATLAS", "images/fx4te.xml"),
	Asset("IMAGE", "images/fx4te.tex"),

	Asset("ANIM", "anim/player_actions_paddle.zip"),
	Asset("ANIM", "anim/player_actions_speargun.zip"),
	Asset("ANIM", "anim/player_actions_tap.zip"),
	Asset("ANIM", "anim/player_actions_panning.zip"),
	Asset("ANIM", "anim/player_actions_hand_lens.zip"),
	Asset("ANIM", "anim/player_mount_actions_speargun.zip"),
	Asset("ANIM", "anim/walani_paddle.zip"),
	Asset("ANIM", "anim/player_boat_death.zip"),
	Asset("ANIM", "anim/player_sneeze.zip"),
	Asset("ANIM", "anim/des_sail.zip"),
	Asset("ANIM", "anim/player_actions_trawl.zip"),
	Asset("ANIM", "anim/player_actions_machete.zip"),
	Asset("ANIM", "anim/player_actions_shear.zip"),
	Asset("ANIM", "anim/player_actions_cropdust.zip"),
	Asset("ANIM", "anim/ripple_build.zip"),

	Asset("ANIM", "anim/boat_health.zip"),
	Asset("ANIM", "anim/player_actions_telescope.zip"),
	Asset("ANIM", "anim/pig_house_old.zip"),

	Asset("ANIM", "anim/parrot_pirate_intro.zip"),
	Asset("ANIM", "anim/parrot_pirate.zip"),


	Asset("ANIM", "anim/pig_house_sale.zip"),


	Asset("ANIM", "anim/fish2.zip"),
	Asset("ANIM", "anim/fish3.zip"),
	Asset("ANIM", "anim/fish4.zip"),
	Asset("ANIM", "anim/fish5.zip"),
	Asset("ANIM", "anim/fish6.zip"),
	Asset("ANIM", "anim/fish7.zip"),
	Asset("ANIM", "anim/coi.zip"),
	Asset("ANIM", "anim/ballphinocean.zip"),
	Asset("ANIM", "anim/dogfishocean.zip"),
	Asset("ANIM", "anim/goldfish.zip"),
	Asset("ANIM", "anim/salmon.zip"),
	Asset("ANIM", "anim/sharxocean.zip"),
	Asset("ANIM", "anim/swordfishjocean.zip"),
	Asset("ANIM", "anim/swordfishjocean2.zip"),
	Asset("ANIM", "anim/mecfish.zip"),
	Asset("ANIM", "anim/whaleblueocean.zip"),
	Asset("ANIM", "anim/kingfisher_build.zip"),
	Asset("ANIM", "anim/parrot_blue_build.zip"),
	Asset("ANIM", "anim/toucan_hamlet_build.zip"),
	Asset("ANIM", "anim/toucan_build.zip"),
	Asset("ANIM", "anim/parrot_build.zip"),
	Asset("ANIM", "anim/parrot_pirate_build.zip"),
	Asset("ANIM", "anim/cormorant_build.zip"),
	Asset("ANIM", "anim/seagull_build.zip"),
	Asset("ANIM", "anim/quagmire_pigeon_build.zip"),
	Asset("ANIM", "anim/skeletons.zip"),
	-- Asset("ANIM", "anim/fish2.zip"),
	-- Asset("ANIM", "anim/oceanfish_small.zip"),
	-- Asset("ANIM", "anim/oceanfish_small_1.zip"),
	-- Asset("ANIM", "anim/oceanfish_small_2.zip"),
	-- Asset("ANIM", "anim/oceanfish_small_3.zip"),
	-- Asset("ANIM", "anim/oceanfish_small_4.zip"),
	-- Asset("ANIM", "anim/oceanfish_small_5.zip"),
	-- Asset("ANIM", "anim/oceanfish_small_6.zip"),
	-- Asset("ANIM", "anim/oceanfish_small_7.zip"),
	-- Asset("ANIM", "anim/oceanfish_small_8.zip"),
	-- Asset("ANIM", "anim/oceanfish_medium.zip"),
	-- Asset("ANIM", "anim/oceanfish_medium_1.zip"),
	-- Asset("ANIM", "anim/oceanfish_medium_2.zip"),
	-- Asset("ANIM", "anim/oceanfish_medium_3.zip"),
	-- Asset("ANIM", "anim/oceanfish_medium_4.zip"),
	-- Asset("ANIM", "anim/oceanfish_medium_5.zip"),
	-- Asset("ANIM", "anim/oceanfish_medium_6.zip"),
	-- Asset("ANIM", "anim/oceanfish_medium_7.zip"),
	-- Asset("ANIM", "anim/oceanfish_medium_8.zip"),
	Asset("IMAGE", "levels/textures/outro.tex"),
	Asset("IMAGE", "levels/textures/ground_noise_water_deep.tex"),
	Asset("IMAGE", "images/inventoryimages/hamletinventory.tex"),
	Asset("ATLAS", "images/inventoryimages/hamletinventory.xml"),
	Asset("IMAGE", "images/inventoryimages/meated_nettle.tex"),
	Asset("ATLAS", "images/inventoryimages/meated_nettle.xml"),









	Asset("ATLAS", "map_icons/hamleticon.xml"),
	Asset("IMAGE", "map_icons/hamleticon.tex"),
	Asset("ATLAS", "map_icons/creepindedeepicon.xml"),
	Asset("IMAGE", "map_icons/creepindedeepicon.tex"),
	-- Asset("ANIM", "anim/butterflymuffin.zip"),
	-- Asset("IMAGE", "images/tfwp_inventoryimgs.tex"),
	-- Asset("ATLAS", "images/tfwp_inventoryimgs.xml"),
	--Asset("SOUNDPACKAGE", "sound/Hamlet.fev"),
	--Asset("SOUND", "sound/Hamlet.fsb"),


	Asset("IMAGE", "images/names_wilbur.tex"),
	Asset("ATLAS", "images/names_wilbur.xml"),
	Asset("IMAGE", "images/names_woodlegs.tex"),
	Asset("ATLAS", "images/names_woodlegs.xml"),
	Asset("IMAGE", "images/names_walani.tex"),
	Asset("ATLAS", "images/names_walani.xml"),


	Asset("ATLAS", "images/tabs.xml"),
	Asset("IMAGE", "images/tabs.tex"),
	Asset("IMAGE", "images/turfs/turf01-9.tex"),
	Asset("ATLAS", "images/turfs/turf01-9.xml"),

	Asset("IMAGE", "images/turfs/turf01-10.tex"),
	Asset("ATLAS", "images/turfs/turf01-10.xml"),

	Asset("IMAGE", "images/turfs/turf01-11.tex"),
	Asset("ATLAS", "images/turfs/turf01-11.xml"),

	Asset("IMAGE", "images/turfs/turf01-12.tex"),
	Asset("ATLAS", "images/turfs/turf01-12.xml"),

	Asset("IMAGE", "images/turfs/turf01-13.tex"),
	Asset("ATLAS", "images/turfs/turf01-13.xml"),

	Asset("IMAGE", "images/turfs/turf01-14.tex"),
	Asset("ATLAS", "images/turfs/turf01-14.xml"),
	Asset("ANIM", "anim/vagner_over.zip"),
	Asset("ANIM", "anim/leaves_canopy2.zip"),

	Asset("ANIM", "anim/mushroom_tree_yelow.zip"),
	Asset("ANIM", "anim/speedicon.zip"),
}

AddMinimapAtlas("map_icons/creepindedeepicon.xml")
AddMinimapAtlas("map_icons/hamleticon.xml")
AddMinimapAtlas("minimap/loot_pump.xml")


-- if GetModConfigData("gorgeisland") and GetModConfigData("kindofworld") == 15 or GetModConfigData("enableallprefabs") == true then
-- table.insert(PrefabFiles, "quagmire_mealingstone")
-- table.insert(PrefabFiles, "quagmire_flour")
-- table.insert(PrefabFiles, "quagmire_foods")
-- table.insert(PrefabFiles, "quagmire_goatmilk")
-- table.insert(PrefabFiles, "quagmire_sap")
-- table.insert(PrefabFiles, "quagmire_syrup")

-- table.insert(PrefabFiles, "quagmire_casseroledish")
-- table.insert(PrefabFiles, "quagmire_crates")
-- table.insert(PrefabFiles, "quagmire_grill")
-- table.insert(PrefabFiles, "quagmire_oven")
-- table.insert(PrefabFiles, "quagmire_plates")
-- table.insert(PrefabFiles, "quagmire_pot")
-- table.insert(PrefabFiles, "quagmire_pot_hanger")
-- table.insert(PrefabFiles, "quagmire_salt_rack")
-- table.insert(PrefabFiles, "quagmire_sapbucket")
-- table.insert(PrefabFiles, "quagmire_slaughtertool")
-- table.insert(PrefabFiles, "quagmire_altar")
-- table.insert(PrefabFiles, "quagmire_seedpackets")

-- table.insert(PrefabFiles, "quagmire_sugarwoodtree")
-- table.insert(PrefabFiles, "quagmire_sugarwood_sapling")

-- table.insert(PrefabFiles, "quagmire_goatmum")
-- table.insert(PrefabFiles, "quagmire_swampigelder")

-- table.insert(PrefabFiles, "quagmire_oldstructures")
-- table.insert(PrefabFiles, "quagmire_lamp_post")
-- table.insert(PrefabFiles, "quagmire_altar_statue")
-- table.insert(PrefabFiles, "quagmire_portal")

-- table.insert(Assets, Asset("ATLAS", "images/inventoryimages/quagmirefoods.xml"))
-- table.insert(Assets, Asset("IMAGE", "images/inventoryimages/quagmirefoods.tex"))
-- end


RegisterInventoryItemAtlas("images/inventoryimages/hamletinventory.xml", "limpets_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/hamletinventory.xml", "limpets.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "coconut_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "coconut_halved.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "coffeebeans.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "coffeebeans_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "sweet_potato.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "sweet_potatos_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "fish_med.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "dead_swordfish.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "fish_raw.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "fish_med_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "quagmire_crabmeat.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "quagmire_crabmeat_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "lobster_land.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "lobster_dead.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "lobster_dead_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "fish_dogfish.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "mussel_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "mussel.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "shark_fin.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "crab.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "seaweed.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "seaweed_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "seaweed_dried.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "doydoyegg.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "dorsalfin.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "jellyfish.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "jellyfish_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "jellyfish_dead.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "jellyjerky.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "fish2.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "fish2_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "fish3.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "fish3_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "fish4.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "fish4_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "fish5.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "fish5_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "fish6.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "fish6_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "fish7.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "fish7_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "salmon.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "salmon_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "coi.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "coi_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "snowitem.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "roe.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "roe_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "seataro.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "seataro_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "blueberries.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "blueberries_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "seacucumber.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "seacucumber_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "gooseberry.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "gooseberry_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "quagmire_mushrooms.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "quagmire_mushrooms_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "oceanfish_small_61_inv.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "oceanfish_small_61_inv_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "oceanfish_small_71_inv.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "oceanfish_small_71_inv_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "oceanfish_small_81_inv.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "oceanfish_small_81_inv_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "butterfly_tropical_wings.tex")

RegisterInventoryItemAtlas("images/inventoryimages/hamletinventory.xml", "jellybug.tex")
RegisterInventoryItemAtlas("images/inventoryimages/hamletinventory.xml", "jellybug_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/hamletinventory.xml", "slugbug.tex")
RegisterInventoryItemAtlas("images/inventoryimages/hamletinventory.xml", "slugbug_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/hamletinventory.xml", "cutnettle.tex")
RegisterInventoryItemAtlas("images/inventoryimages/hamletinventory.xml", "radish.tex")
RegisterInventoryItemAtlas("images/inventoryimages/hamletinventory.xml", "radish_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/hamletinventory.xml", "asparagus.tex")
RegisterInventoryItemAtlas("images/inventoryimages/hamletinventory.xml", "asparagus_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/hamletinventory.xml", "aloe.tex")
RegisterInventoryItemAtlas("images/inventoryimages/hamletinventory.xml", "aloe_cooked.tex")
RegisterInventoryItemAtlas("images/inventoryimages/hamletinventory.xml", "weevole_carapace.tex")
RegisterInventoryItemAtlas("images/inventoryimages/hamletinventory.xml", "piko_orange.tex")
RegisterInventoryItemAtlas("images/inventoryimages/hamletinventory.xml", "snake_bone.tex")

----------------新的图片
-- RegisterInventoryItemAtlas("images/inventoryimages/tap_buildingimages.xml", "kyno_fountainyouth.tex")
