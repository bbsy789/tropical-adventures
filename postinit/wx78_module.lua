local wx78_moduledefs = require("wx78_moduledefs")
local module_definitions = wx78_moduledefs.module_definitions
local AddCreatureScanDataDefinition = wx78_moduledefs.AddCreatureScanDataDefinition
AddCreatureScanDataDefinition("crab", "movespeed", 2)
AddCreatureScanDataDefinition("piko", "movespeed", 2)

AddCreatureScanDataDefinition("twister", "movespeed2", 6)
AddCreatureScanDataDefinition("rookwater", "movespeed2", 3)
AddCreatureScanDataDefinition("pangolden", "movespeed2", 1)
AddCreatureScanDataDefinition("wildbore", "movespeed2", 1)

AddCreatureScanDataDefinition("butterfly_tropical", "maxsanity1", 1)
AddCreatureScanDataDefinition("glowfly", "maxsanity1", 1)

AddCreatureScanDataDefinition("ancient_herald", "maxsanity", 6)

AddCreatureScanDataDefinition("crocodog", "maxhunger1", 2)
AddCreatureScanDataDefinition("pog", "maxhunger1", 2)

AddCreatureScanDataDefinition("tigershark", "maxhunger", 6)
AddCreatureScanDataDefinition("spider_monkey", "maxhunger", 3)

AddCreatureScanDataDefinition("glowfly", "light", 1)
AddCreatureScanDataDefinition("bioluminescenc", "light", 1)

AddCreatureScanDataDefinition("dragoon", "heat", 2)
AddCreatureScanDataDefinition("scorpion", "heat", 1)

AddCreatureScanDataDefinition("watercrocodog", "cold", 4)
AddCreatureScanDataDefinition("hippopotamoose", "cold", 4)

AddCreatureScanDataDefinition("bioluminescence", "nightvision", 1)
AddCreatureScanDataDefinition("vampirebat", "nightvision", 2)

AddCreatureScanDataDefinition("antqueen", "bee", 10)

AddCreatureScanDataDefinition("mandrakeman", "music", 2)
AddCreatureScanDataDefinition("whale_blue", "music", 4)
AddCreatureScanDataDefinition("whale_blue", "whale_white", 8)

AddCreatureScanDataDefinition("jellyfish_planted", "taser", 1)
AddCreatureScanDataDefinition("thunderbird", "taser", 2)
