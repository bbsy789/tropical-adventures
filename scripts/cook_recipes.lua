AddIngredientValues({ "butterfly_tropical_wings" }, { veggie = 0.5 }, true, false)

AddIngredientValues({ "limpets_cooked" }, { fish = 0.5 }, true, false)
AddIngredientValues({ "limpets" }, { fish = 0.5 }, true, false)
AddIngredientValues({ "coconut_cooked", "coconut_halved" }, { fruit = 1, fat = 1 }, true, false)
AddIngredientValues({ "coffeebeans" }, { fruit = .5 }, true, false)
AddIngredientValues({ "coffeebeans_cooked" }, { fruit = 1 }, true, false)
AddIngredientValues({ "sweet_potato" }, { veggie = 1 }, true, false)
AddIngredientValues({ "sweet_potatos_cooked" }, { veggie = 1 }, true, false)
AddIngredientValues({ "fish_med" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "dead_swordfish" }, { fish = 1.5 }, true, false)
AddIngredientValues({ "fish_raw" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "fish_med_cooked" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "quagmire_crabmeat" }, { fish = 0.5, crab = 1 }, true, false)
AddIngredientValues({ "quagmire_crabmeat_cooked" }, { fish = 0.5, crab = 1 }, true, false)
AddIngredientValues({ "lobster_land" }, { meat = 1.0, fish = 1.0 }, true, false)
AddIngredientValues({ "lobster_dead" }, { meat = 1.0, fish = 1.0 }, true, false)
AddIngredientValues({ "lobster_dead_cooked" }, { meat = 1.0, fish = 1.0 }, true, false)
AddIngredientValues({ "fish_dogfish" }, { fish = 1 }, true, false)
AddIngredientValues({ "mussel_cooked" }, { fish = 0.5 }, true, false)
AddIngredientValues({ "mussel" }, { fish = 0.5 }, true, false)
AddIngredientValues({ "shark_fin" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "crab" }, { fish = 0.5 }, true, false)
AddIngredientValues({ "seaweed" }, { veggie = 1 }, true, false)
AddIngredientValues({ "seaweed_cooked" }, { veggie = 1 }, true, false)
AddIngredientValues({ "seaweed_dried" }, { veggie = 1 }, true, false)
AddIngredientValues({ "doydoyegg" }, { egg = 1 }, true, false)
AddIngredientValues({ "dorsalfin" }, { inedible = 1 }, true, false)
AddIngredientValues({ "jellyfish" }, { fish = 1, jellyfish = 1, monster = 1 }, true, false)
AddIngredientValues({ "jellyfish_cooked" }, { fish = 1, jellyfish = 1, monster = 1 }, true, false)
AddIngredientValues({ "jellyfish_dead" }, { fish = 1, jellyfish = 1, monster = 1 }, true, false)
AddIngredientValues({ "jellyjerky" }, { fish = 1, jellyfish = 1, monster = 1 }, true, false)

AddIngredientValues({ "fish2" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "fish2_cooked" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "fish3" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "fish3_cooked" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "fish4" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "fish4_cooked" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "fish5" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "fish5_cooked" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "fish6" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "fish6_cooked" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "fish7" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "fish7_cooked" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "salmon" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "salmon_cooked" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "coi" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "coi_cooked" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "snowitem" }, { meat = 0.5, frozen = 1 }, true, false)

AddIngredientValues({ "roe" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "roe_cooked" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "quagmire_spotspice_sprig" }, { veggie = 1 }, true, false)
AddIngredientValues({ "quagmire_sap" }, { sweetener = 1 }, true, false)

AddIngredientValues({ "seataro" }, { veggie = 1, frozen = 1 }, true, false)
AddIngredientValues({ "seataro_cooked" }, { veggie = 1, frozen = 1 }, true, false)

AddIngredientValues({ "blueberries" }, { fruit = 0.5, frozen = 0.25 }, true, false)
AddIngredientValues({ "blueberries_cooked" }, { fruit = 0.75 }, true, false)

AddIngredientValues({ "seacucumber" }, { veggie = 1 }, true, false)
AddIngredientValues({ "seacucumber_cooked" }, { veggie = 1 }, true, false)

AddIngredientValues({ "gooseberry" }, { veggie = 1 }, true, false)
AddIngredientValues({ "gooseberry_cooked" }, { veggie = 1 }, true, false)

AddIngredientValues({ "quagmire_mushrooms" }, { mushroom = 1, veggie = 0.5 }, true, false)
AddIngredientValues({ "quagmire_mushrooms_cooked" }, { mushroom = 1, veggie = 0.5 }, true, false)

AddIngredientValues({ "oceanfish_small_61_inv" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "oceanfish_small_61_inv_cooked" }, { meat = 0.5, fish = 1 }, true, false)

AddIngredientValues({ "oceanfish_small_71_inv" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "oceanfish_small_71_inv_cooked" }, { meat = 0.5, fish = 1 }, true, false)

AddIngredientValues({ "oceanfish_small_81_inv" }, { meat = 0.5, fish = 1 }, true, false)
AddIngredientValues({ "oceanfish_small_81_inv_cooked" }, { meat = 0.5, fish = 1 }, true, false)

AddIngredientValues({ "foliage" }, { veggie = 1 }, true, false)
AddIngredientValues({ "jellybug" }, { bug = 1 }, true, false)
AddIngredientValues({ "jellybug_cooked" }, { bug = 1 }, true, false)
AddIngredientValues({ "slugbug" }, { bug = 1 }, true, false)
AddIngredientValues({ "slugbug_cooked" }, { bug = 1 }, true, false)
AddIngredientValues({ "cutnettle" }, { antihistamine = 1 }, true, false)
AddIngredientValues({ "radish" }, { veggie = 1 }, true, false)
AddIngredientValues({ "radish_cooked" }, { veggie = 1 }, true, false)

AddIngredientValues({ "turnip" }, { veggie = 1 }, true, false)
AddIngredientValues({ "turnip_cooked" }, { veggie = 1 }, true, false)

AddIngredientValues({ "asparagus" }, { veggie = 1 }, true, false)
AddIngredientValues({ "asparagus_cooked" }, { veggie = 1 }, true, false)
AddIngredientValues({ "aloe" }, { veggie = 1 }, true, false)
AddIngredientValues({ "aloe_cooked" }, { veggie = 1 }, true, false)
AddIngredientValues({ "weevole_carapace" }, { inedible = 1 }, true, false)
AddIngredientValues({ "piko_orange" }, { filter = 1 }, true, false)
AddIngredientValues({ "snake_bone" }, { bone = 1 }, true, false)
AddIngredientValues({ "yelow_cap" }, { veggie = 0.5 }, true, false)
AddIngredientValues({ "yelow_cooked" }, { veggie = 0.5 }, true, false)

local fruityjuice =
{
	name = "fruityjuice",
	test = function(cooker, names, tags)
		return names.blueberries_cooked and names.blueberries_cooked == 2 and
			names.foliage and tags.frozen or
			names.blueberries and names.blueberries == 2 and names.foliage and tags.frozen
	end,
	priority = 1,
	weight = 1,
	foodtype = "VEGGIE",
	health = TUNING.HEALING_MED,
	hunger = TUNING.CALORIES_LARGE,
	sanity = TUNING.SANITY_TINY,
	cooktime = 2,
	floater = { "small", 0.05, 0.7 },
	tags = {},
	cookbook_atlas = "images/inventoryimages/volcanoinventory.xml"
}

AddCookerRecipe("cookpot", fruityjuice)
AddCookerRecipe("portablecookpot", fruityjuice)
-------------------AddCookerRecipe("xiuyuan_cookpot", fruityjuice)


local butterflymuffin =
{
	name = "butterflymuffin",
	test = function(cooker, names, tags)
		return (names.butterflywings or names.butterfly_tropical_wings or names.moonbutterflywings) and
			not tags.meat and tags.veggie
	end,
	priority = 1,
	weight = 1,
	foodtype = "VEGGIE",
	health = TUNING.HEALING_MED,
	hunger = TUNING.CALORIES_LARGE,
	sanity = TUNING.SANITY_TINY,
	cooktime = 2,
	floater = { "small", 0.05, 0.7 },
	tags = {},
	cookbook_atlas = "images/inventoryimages/volcanoinventory.xml"
}

AddCookerRecipe("cookpot", butterflymuffin)
AddCookerRecipe("portablecookpot", butterflymuffin)
-------------------AddCookerRecipe("xiuyuan_cookpot", butterflymuffin)

local coffee =
{
	name = "coffee",
	test = function(cooker, names, tags)
		return names.coffeebeans_cooked and
			(names.coffeebeans_cooked == 4 or (names.coffeebeans_cooked == 3 and (tags.dairy or tags.sweetener)))
	end,
	priority = 30,
	weight = 1,
	foodtype = "VEGGIE",
	health = 3,
	hunger = 75 / 8,
	sanity = -5,
	cooktime = .5,
	tags = {},
	cookbook_atlas = "images/inventoryimages/volcanoinventory.xml",
	oneat_desc = "Speeds the body",
}
AddCookerRecipe("cookpot", coffee)
AddCookerRecipe("portablecookpot", coffee)
-------------------AddCookerRecipe("xiuyuan_cookpot", coffee)
--[[
local surfnturf =
{
    name = "surfnturf",
		test = function(cooker, names, tags) return tags.meat and tags.meat >= 2.5 and tags.fish and tags.fish >= 1.5 and not tags.frozen end,
		priority = 30,
		weight = 1,
		foodtype = "MEAT",
		health = 60,
		hunger = 75/2,
		perishtime = 10*480,
		sanity = 33,
		cooktime = 1,
}
AddCookerRecipe("cookpot",surfnturf)
AddCookerRecipe("portablecookpot",surfnturf)

local seafoodgumbo =
{
    name = "seafoodgumbo",
		test = function(cooker, names, tags) return tags.fish and tags.fish > 2 end,
		priority = 10,
		weight = 1,
		foodtype = "MEAT",
		health = 40,
		hunger = 75/2,
		perishtime = 10*480,
		sanity = 20,
		cooktime = 1,
}
AddCookerRecipe("cookpot",seafoodgumbo)
AddCookerRecipe("portablecookpot",seafoodgumbo)

local ceviche =
{
    name = "ceviche",
		test = function(cooker, names, tags) return tags.fish and tags.fish >= 2 and tags.frozen end,
		priority = 20,
		weight = 1,
		foodtype = "MEAT",
		health = 20,
		hunger = 75/3,
		perishtime = 10*480,
		sanity = 5,
		temperature = -40,
		temperatureduration = 10,
		cooktime = 0.5,
}
AddCookerRecipe("cookpot",ceviche)
AddCookerRecipe("portablecookpot",ceviche)

local freshfruitcrepes =
{
    name = "freshfruitcrepes",
	test = function(cooker, names, tags) return tags.fruit and tags.fruit >= 1.5 and names.butter and names.honey end,
	priority = 30,
	weight = 1,
	foodtype = "VEGGIE",
	health = TUNING.HEALING_HUGE,
	hunger = TUNING.CALORIES_SUPERHUGE,
	perishtime = TUNING.PERISH_MED,
	sanity = TUNING.SANITY_MED,
	cooktime = 2,	
}
AddCookerRecipe("portablecookpot",freshfruitcrepes)




local monstertartare =
	{
	name = "monstertartare",
	test = function(cooker, names, tags) return tags.monster and tags.monster >= 2 and tags.egg and tags.veggie end,
	priority = 30,
	   weight = 1,
	foodtype = "MEAT",
	health = TUNING.HEALING_SMALL,
	hunger = TUNING.CALORIES_LARGE,
	perishtime = TUNING.PERISH_MED,
	sanity = TUNING.SANITY_TINY,
	cooktime = 2,
}
AddCookerRecipe("portablecookpot",monstertartare)



]]
local musselbouillabaise =
{
	name = "musselbouillabaise",
	test = function(cooker, names, tags) return names.mussel and names.mussel == 2 and tags.veggie and tags.veggie >= 2 end,
	priority = 30,
	weight = 1,
	foodtype = "MEAT",
	health = TUNING.HEALING_MED,
	hunger = TUNING.CALORIES_LARGE,
	perishtime = TUNING.PERISH_MED,
	sanity = TUNING.SANITY_MED,
	cooktime = 2,
	tags = { "masterfood" },
	cookbook_atlas = "images/inventoryimages/volcanoinventory.xml"
}
AddCookerRecipe("portablecookpot", musselbouillabaise)

local sweetpotatosouffle =
{
	name = "sweetpotatosouffle",
	test = function(cooker, names, tags)
		return names.sweet_potato and names.sweet_potato == 2 and tags.egg and
			tags.egg >= 2
	end,
	priority = 30,
	weight = 1,
	foodtype = "VEGGIE",
	health = TUNING.HEALING_MED,
	hunger = TUNING.CALORIES_LARGE,
	perishtime = TUNING.PERISH_MED,
	sanity = TUNING.SANITY_MED,
	cooktime = 2,
	tags = { "masterfood" },
	cookbook_atlas = "images/inventoryimages/volcanoinventory.xml"
}
AddCookerRecipe("portablecookpot", sweetpotatosouffle)

local sharkfinsoup =
{
	name = "sharkfinsoup",
	test = function(cooker, names, tags) return names.shark_fin end,
	priority = 20,
	weight = 1,
	foodtype = "MEAT",
	health = 40,
	hunger = 75 / 6,
	perishtime = 10 * 480,
	sanity = -10,
	naughtiness = 10,
	cooktime = 1,
	cookbook_atlas = "images/inventoryimages/volcanoinventory.xml"
}
AddCookerRecipe("cookpot", sharkfinsoup)
AddCookerRecipe("portablecookpot", sharkfinsoup)
-------------------AddCookerRecipe("xiuyuan_cookpot", sharkfinsoup)

local lobsterdinner =
{
	name = "lobsterdinner",
	test = function(cooker, names, tags)
		return (names.lobster_dead or names.wobster_sheller_land or names.lobster_dead_cooked or names.lobster_land) and
			names.butter and (tags.meat == 1.0) and (tags.fish == 1.0) and not tags.frozen
	end,
	priority = 25,
	weight = 1,
	foodtype = "MEAT",
	health = TUNING.HEALING_HUGE,
	hunger = TUNING.CALORIES_LARGE,
	perishtime = TUNING.PERISH_SLOW,
	sanity = TUNING.SANITY_HUGE,
	cooktime = 1,
	overridebuild = "cook_pot_food3",
	potlevel = "high",
	floater = { "med", 0.05, { 0.65, 0.6, 0.65 } },
	cookbook_atlas = "images/inventoryimages/volcanoinventory.xml"
}
AddCookerRecipe("cookpot", lobsterdinner)
AddCookerRecipe("portablecookpot", lobsterdinner)
-------------------AddCookerRecipe("xiuyuan_cookpot", lobsterdinner)

local lobsterbisque =
{
	name = "lobsterbisque",
	test = function(cooker, names, tags)
		return (names.lobster_dead or names.lobster_dead_cooked or names.lobster_land or names.wobster_sheller_land) and
			tags.frozen
	end,
	priority = 30,
	weight = 1,
	foodtype = "MEAT",
	health = TUNING.HEALING_HUGE,
	hunger = TUNING.CALORIES_MED,
	perishtime = TUNING.PERISH_MED,
	sanity = TUNING.SANITY_SMALL,
	cooktime = 0.5,
	overridebuild = "cook_pot_food3",
	potlevel = "high",
	floater = { "med", 0.05, { 0.65, 0.6, 0.65 } },
	cookbook_atlas = "images/inventoryimages/volcanoinventory.xml"
}
AddCookerRecipe("cookpot", lobsterbisque)
AddCookerRecipe("portablecookpot", lobsterbisque)
-------------------AddCookerRecipe("xiuyuan_cookpot", lobsterdinner)

local jellyopop =
{
	name = "jellyopop",
	test = function(cooker, names, tags) return tags.jellyfish and tags.frozen and tags.inedible end,
	priority = 20,
	weight = 1,
	foodtype = "MEAT",
	health = 20,
	hunger = 75 / 6,
	perishtime = 3 * 480,
	sanity = 0,
	temperature = -40,
	temperatureduration = 10,
	cooktime = 0.5,
	cookbook_atlas = "images/inventoryimages/volcanoinventory.xml"
}
AddCookerRecipe("cookpot", jellyopop)
AddCookerRecipe("portablecookpot", jellyopop)
-------------------AddCookerRecipe("xiuyuan_cookpot", jellyopop)

local californiaroll =
{
	name = "californiaroll",
	test = function(cooker, names, tags)
		return ((names.kelp or 0) + (names.kelp_cooked or 0) + (names.kelp_dried or 0) + (names.seaweed or 0) + (names.kelp_dried or 0)) ==
			2 and (tags.fish and tags.fish >= 1)
	end,
	priority = 20,
	weight = 1,
	foodtype = "MEAT",
	health = TUNING.HEALING_MED,
	hunger = TUNING.CALORIES_LARGE,
	perishtime = TUNING.PERISH_MED,
	sanity = TUNING.SANITY_SMALL,
	cooktime = .5,
	overridebuild = "cook_pot_food2",
	potlevel = "high",
	floater = { "med", 0.05, { 0.65, 0.6, 0.65 } },
	cookbook_atlas = "images/inventoryimages/volcanoinventory.xml"
}
AddCookerRecipe("cookpot", californiaroll)
AddCookerRecipe("portablecookpot", californiaroll)
-------------------AddCookerRecipe("xiuyuan_cookpot", californiaroll)

local bisque =
{
	name = "bisque",
	test = function(cooker, names, tags) return names.limpets and names.limpets == 3 and tags.frozen end,
	priority = 30,
	weight = 1,
	foodtype = "MEAT",
	health = 60,
	hunger = 75 / 4,
	perishtime = 10 * 480,
	sanity = 5,
	cooktime = 1,
	cookbook_atlas = "images/inventoryimages/volcanoinventory.xml"
}
AddCookerRecipe("cookpot", bisque)
AddCookerRecipe("portablecookpot", bisque)
-------------------AddCookerRecipe("xiuyuan_cookpot", bisque)

local bananapop =
{
	name = "bananapop",
	test = function(cooker, names, tags)
		return (names.cave_banana or names.cave_banana_cooked) and tags.frozen and
			(tags.inedible or names.twigs) and not tags.meat and not tags.fish and (tags.inedible and tags.inedible <= 2)
	end,
	priority = 20,
	weight = 1,
	foodtype = "VEGGIE",
	health = TUNING.HEALING_MED,
	hunger = TUNING.CALORIES_SMALL,
	perishtime = TUNING.PERISH_SUPERFAST,
	sanity = TUNING.SANITY_LARGE,
	temperature = TUNING.COLD_FOOD_BONUS_TEMP,
	temperatureduration = TUNING.FOOD_TEMP_AVERAGE,
	cooktime = 0.5,
	potlevel = "low",
	floater = { nil, 0.05, 0.95 },
	cookbook_atlas = "images/inventoryimages/volcanoinventory.xml"
}
AddCookerRecipe("cookpot", bananapop)
AddCookerRecipe("portablecookpot", bananapop)
-------------------AddCookerRecipe("xiuyuan_cookpot", bananapop)


local caviar =
{
	name = "caviar",
	test = function(cooker, names, tags) return (names.roe or names.roe_cooked == 3) and tags.veggie end,
	priority = 20,
	weight = 1,
	foodtype = "MEAT",
	health = TUNING.HEALING_SMAL,
	hunger = TUNING.CALORIES_SMALL,
	perishtime = TUNING.PERISH_MED,
	sanity = TUNING.SANITY_LARGE,
	cooktime = 2,
	cookbook_atlas = "images/inventoryimages/volcanoinventory.xml"
}
AddCookerRecipe("cookpot", caviar)
AddCookerRecipe("portablecookpot", caviar)
-------------------AddCookerRecipe("xiuyuan_cookpot", caviar)

local tropicalbouillabaisse =
{
	name = "tropicalbouillabaisse",
	test = function(cooker, names, tags)
		return (names.fish3 or names.fish3_cooked) and
			(names.fish4 or names.fish4_cooked) and (names.fish5 or names.fish5_cooked) and tags.veggie
	end,
	priority = 35,
	weight = 1,
	foodtype = "MEAT",
	health = TUNING.HEALING_MED,
	hunger = TUNING.CALORIES_LARGE,
	perishtime = TUNING.PERISH_MED,
	sanity = TUNING.SANITY_MED,
	cooktime = 2,
	cookbook_atlas = "images/inventoryimages/volcanoinventory.xml"
}
AddCookerRecipe("cookpot", tropicalbouillabaisse)
AddCookerRecipe("portablecookpot", tropicalbouillabaisse)
-------------------AddCookerRecipe("xiuyuan_cookpot", tropicalbouillabaisse)

local spicyvegstinger =
{
	name = "spicyvegstinger",
	test = function(cooker, names, tags)
		return (names.asparagus or names.asparagus_cooked or names.radish or names.radish_cooked) and
			tags.veggie and tags.veggie > 2 and tags.frozen and not tags.meat
	end,
	priority = 15,
	weight = 1,
	foodtype = "VEGGIE",
	health = TUNING.HEALING_SMALL,
	hunger = TUNING.CALORIES_MED,
	perishtime = TUNING.PERISH_SLOW,
	sanity = TUNING.SANITY_LARGE,
	cooktime = 0.5,
}
AddCookerRecipe("cookpot", spicyvegstinger)
AddCookerRecipe("portablecookpot", spicyvegstinger)
-------------------AddCookerRecipe("xiuyuan_cookpot", spicyvegstinger)

local feijoada =
{
	name = "feijoada",
	test = function(cooker, names, tags)
		return tags.meat and (names.jellybug == 3) or (names.jellybug_cooked == 3) or
			(names.jellybug and names.jellybug_cooked and names.jellybug + names.jellybug_cooked == 3)
	end,
	priority = 30,
	weight = 1,
	foodtype = "MEAT",
	health = TUNING.HEALING_MED,
	hunger = TUNING.CALORIES_HUGE,
	perishtime = TUNING.PERISH_FASTISH,
	sanity = TUNING.SANITY_MED,
	cooktime = 3.5,
	cookbook_atlas = "images/inventoryimages/hamletinventory.xml",
}
AddCookerRecipe("cookpot", feijoada)
AddCookerRecipe("portablecookpot", feijoada)
-------------------AddCookerRecipe("xiuyuan_cookpot", feijoada)

local steamedhamsandwich =
{
	name = "steamedhamsandwich",
	test = function(cooker, names, tags)
		return (names.meat or names.meat_cooked) and (tags.veggie and tags.veggie >= 2) and
			names.foliage
	end,
	priority = 5,
	weight = 1,
	foodtype = "MEAT",
	health = TUNING.HEALING_LARGE,
	hunger = TUNING.CALORIES_LARGE,
	perishtime = TUNING.PERISH_FAST,
	sanity = TUNING.SANITY_MED,
	cooktime = 2,
	cookbook_atlas = "images/inventoryimages/hamletinventory.xml",
}
AddCookerRecipe("cookpot", steamedhamsandwich)
AddCookerRecipe("portablecookpot", steamedhamsandwich)
-------------------AddCookerRecipe("xiuyuan_cookpot", steamedhamsandwich)

local hardshell_tacos =
{
	name = "hardshell_tacos",
	test = function(cooker, names, tags) return (names.weevole_carapace == 2) and tags.veggie end,
	priority = 1,
	weight = 1,
	foodtype = "VEGGIE",
	health = TUNING.HEALING_MED,
	hunger = TUNING.CALORIES_LARGE,
	perishtime = TUNING.PERISH_SLOW,
	sanity = TUNING.SANITY_TINY,
	cooktime = 1,
	cookbook_atlas = "images/inventoryimages/hamletinventory.xml",
}
AddCookerRecipe("cookpot", hardshell_tacos)
AddCookerRecipe("portablecookpot", hardshell_tacos)
-------------------AddCookerRecipe("xiuyuan_cookpot", hardshell_tacos)

local gummy_cake =
{
	name = "gummy_cake",
	test = function(cooker, names, tags) return (names.slugbug or names.slugbug_cooked) and tags.sweetener end,
	priority = 1,
	weight = 1,
	foodtype = "MEAT",
	health = -TUNING.HEALING_SMALL,
	hunger = TUNING.CALORIES_SUPERHUGE,
	perishtime = TUNING.PERISH_PRESERVED,
	sanity = -TUNING.SANITY_TINY,
	cooktime = 2,
	cookbook_atlas = "images/inventoryimages/hamletinventory.xml",
}
AddCookerRecipe("cookpot", gummy_cake)
AddCookerRecipe("portablecookpot", gummy_cake)
-------------------AddCookerRecipe("xiuyuan_cookpot", gummy_cake)

local tea =
{
	name = "tea",
	test = function(cooker, names, tags)
		return tags.filter and tags.filter >= 2 and tags.sweetener and not tags.meat and
			not tags.veggie and not tags.inedible
	end,
	priority = 25,
	weight = 1,
	foodtype = "VEGGIE",
	health = TUNING.HEALING_SMALL,
	hunger = TUNING.CALORIES_SMALL,
	perishtime = TUNING.PERISH_ONE_DAY,
	sanity = TUNING.SANITY_LARGE,
	cooktime = 0.5,
	cookbook_atlas = "images/inventoryimages/hamletinventory.xml",
}
AddCookerRecipe("cookpot", tea)
AddCookerRecipe("portablecookpot", tea)
-------------------AddCookerRecipe("xiuyuan_cookpot", tea)

local icedtea =
{
	name = "icedtea",
	test = function(cooker, names, tags) return tags.filter and tags.filter >= 2 and tags.sweetener and tags.frozen end,
	priority = 30,
	weight = 1,
	foodtype = "VEGGIE",
	health = TUNING.HEALING_SMALL,
	hunger = TUNING.CALORIES_SMALL,
	perishtime = TUNING.PERISH_FAST,
	sanity = TUNING.SANITY_LARGE,
	cooktime = 0.5,
	cookbook_atlas = "images/inventoryimages/hamletinventory.xml",
}
AddCookerRecipe("cookpot", icedtea)
AddCookerRecipe("portablecookpot", icedtea)
-------------------AddCookerRecipe("xiuyuan_cookpot", icedtea)

local snakebonesoup =
{
	name = "snakebonesoup",
	test = function(cooker, names, tags) return tags.bone and tags.bone >= 2 and tags.meat and tags.meat >= 2 end,
	priority = 20,
	weight = 1,
	foodtype = "MEAT",
	health = TUNING.HEALING_LARGE,
	hunger = TUNING.CALORIES_MED,
	perishtime = TUNING.PERISH_MED,
	sanity = TUNING.SANITY_SMALL,
	cooktime = 1,
	cookbook_atlas = "images/inventoryimages/hamletinventory.xml",
}
AddCookerRecipe("cookpot", snakebonesoup)
AddCookerRecipe("portablecookpot", snakebonesoup)
-------------------AddCookerRecipe("xiuyuan_cookpot", snakebonesoup)

local nettlelosange =
{
	name = "nettlelosange",
	test = function(cooker, names, tags) return tags.antihistamine and tags.antihistamine >= 3 end,
	priority = 0,
	weight = 1,
	health = TUNING.HEALING_MED,
	hunger = TUNING.CALORIES_MED,
	perishtime = TUNING.PERISH_FAST,
	sanity = TUNING.SANITY_TINY,
	antihistamine = 720,
	cooktime = .5,
	cookbook_atlas = "images/inventoryimages/hamletinventory.xml",
}
AddCookerRecipe("cookpot", nettlelosange)
AddCookerRecipe("portablecookpot", nettlelosange)
-------------------AddCookerRecipe("xiuyuan_cookpot", nettlelosange)


local meated_nettle =
{
	name = "meated_nettle",
	test = function(cooker, names, tags)
		return (tags.antihistamine and tags.antihistamine >= 2) and
			(tags.meat and tags.meat >= 1) and (not tags.monster or tags.monster <= 1) and not tags.inedible
	end,
	priority = 1,
	weight = 1,
	foodtype = "MEAT",
	health = TUNING.HEALING_MED,
	hunger = TUNING.CALORIES_LARGE,
	perishtime = TUNING.PERISH_FASTISH,
	sanity = TUNING.SANITY_TINY,
	antihistamine = 600,
	cooktime = 1,
	cookbook_atlas = "images/inventoryimages/meated_nettle.xml",
}
AddCookerRecipe("cookpot", meated_nettle)
AddCookerRecipe("portablecookpot", meated_nettle)
-------------------AddCookerRecipe("xiuyuan_cookpot", meated_nettle)
