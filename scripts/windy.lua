
table.insert(PrefabFiles,"peach_trees")
table.insert(PrefabFiles,"peach")
table.insert(PrefabFiles,"peach_pit")
table.insert(PrefabFiles,"peachy_poop")
table.insert(PrefabFiles,"windyfan")
table.insert(PrefabFiles,"usedfan")
table.insert(PrefabFiles,"goddess_statue")
table.insert(PrefabFiles,"goddess_shrine")
table.insert(PrefabFiles,"goddess_flute")
table.insert(PrefabFiles,"goddess_flower")
table.insert(PrefabFiles,"goddess_feather")
table.insert(PrefabFiles,"forbidden_fruit")
table.insert(PrefabFiles,"goddess_spider_hostile")
table.insert(PrefabFiles,"goddess_staff")
table.insert(PrefabFiles,"goddess_bird")
table.insert(PrefabFiles,"goddess_butterfly")
table.insert(PrefabFiles,"goddess_butterflywings")
table.insert(PrefabFiles,"greengrass")
table.insert(PrefabFiles,"cutgreengrass")
table.insert(PrefabFiles,"magicpowder")
table.insert(PrefabFiles,"goddess_gate1")
table.insert(PrefabFiles,"goddess_statue2")
table.insert(PrefabFiles,"goddess_statue3")
table.insert(PrefabFiles,"goddess_statue4")
table.insert(PrefabFiles,"goddess_statue5")
table.insert(PrefabFiles,"goddess_statue_fire")
table.insert(PrefabFiles,"page")
table.insert(PrefabFiles,"goddess_figure")
table.insert(PrefabFiles,"peach_smoothie")
table.insert(PrefabFiles,"grilled_peach")
table.insert(PrefabFiles,"caramel_peach")
table.insert(PrefabFiles,"peach_kabobs")
table.insert(PrefabFiles,"goddess_bowtie")
table.insert(PrefabFiles,"gem_flower")
table.insert(PrefabFiles,"gem_seeds")
table.insert(PrefabFiles,"goddess_sword")
table.insert(PrefabFiles,"goddess_hat")
table.insert(PrefabFiles,"goddess_deer2")
table.insert(PrefabFiles,"goddess_ribbon")
table.insert(PrefabFiles,"peachy_meatloaf")
table.insert(PrefabFiles,"peach_custard")
table.insert(PrefabFiles,"mixed_gem")
table.insert(PrefabFiles,"goddess_lantern")
table.insert(PrefabFiles,"goddess_lantern_fire")
table.insert(PrefabFiles,"goddess_rabbit")
table.insert(PrefabFiles,"goddess_rabbithole")
table.insert(PrefabFiles,"goddess_rabbit_fur")
table.insert(PrefabFiles,"green_forcefieldfx")
table.insert(PrefabFiles,"goddess_sparklefx")
table.insert(PrefabFiles,"goddess_magicfx")
table.insert(PrefabFiles,"goddess_fountain")
table.insert(PrefabFiles,"gem_flower1")
table.insert(PrefabFiles,"goddess_deer1")
table.insert(PrefabFiles,"goddess_deer_magic")
table.insert(PrefabFiles,"goddess_fountain_placer")
table.insert(PrefabFiles,"goddess_star")
table.insert(PrefabFiles,"goddess_telefx")
table.insert(PrefabFiles,"empty_bottle_green")
table.insert(PrefabFiles,"full_bottle_green")
table.insert(PrefabFiles,"goddess_lake")
table.insert(PrefabFiles,"goddess_fountain_lake")
table.insert(PrefabFiles,"goddess_pouch")
table.insert(PrefabFiles,"goddess_fish")
table.insert(PrefabFiles,"potion_bottle_green")
table.insert(PrefabFiles,"peach_juice_bottle_green")
table.insert(PrefabFiles,"full_bottle_green_milk")
table.insert(PrefabFiles,"flowers_lake")
table.insert(PrefabFiles,"goddess_bell")
table.insert(PrefabFiles,"glass_bomb")
table.insert(PrefabFiles,"deer_flower")
table.insert(PrefabFiles,"deer_flower_withered")
table.insert(PrefabFiles,"goddess_fishingrod")


	table.insert(Assets, Asset("IMAGE", "levels/textures/noise_windy.tex"))
	table.insert(Assets, Asset("IMAGE", "levels/textures/mini_noise_windy.tex"))
	table.insert(Assets, Asset("IMAGE", "levels/tiles/windy.tex"))
	table.insert(Assets, Asset("FILE", "levels/tiles/windy.xml"))
	
	table.insert(Assets, Asset("IMAGE", "images/map_icons/goddess_statue.tex"))
	table.insert(Assets, Asset("ATLAS", "images/map_icons/goddess_statue.xml"))
	
	table.insert(Assets, Asset("IMAGE", "images/map_icons/goddess_statue2.tex"))
	table.insert(Assets, Asset("ATLAS", "images/map_icons/goddess_statue2.xml"))
	
	table.insert(Assets, Asset("IMAGE", "images/map_icons/goddess_statue3.tex"))
	table.insert(Assets, Asset("ATLAS", "images/map_icons/goddess_statue3.xml"))
	
	table.insert(Assets, Asset("IMAGE", "images/map_icons/goddess_fountain.tex"))
	table.insert(Assets, Asset("ATLAS", "images/map_icons/goddess_fountain.xml"))
	
	table.insert(Assets, Asset("IMAGE", "images/map_icons/goddess_shrine.tex"))
	table.insert(Assets, Asset("ATLAS", "images/map_icons/goddess_shrine.xml"))
	
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/windyfan1.tex"))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/windyfan1.xml"))
	
	table.insert(Assets, Asset("ANIM", "anim/windyfan.zip"))
	table.insert(Assets, Asset("ANIM", "anim/swap_windyfan.zip"))
	
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/peach.tex"))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/peach.xml"))
	
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/forbidden_fruit.tex"))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/forbidden_fruit.xml"))
	
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/goddess_flute.tex"))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/goddess_flute.xml"))
	
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/goddess_staff.tex"))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/goddess_staff.xml"))
	
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/peachy_poop.tex"))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/peachy_poop.xml"))
	
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/goddess_butterfly.tex"))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/goddess_butterfly.xml"))
	
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/cutgreengrass.tex"))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/cutgreengrass.xml"))
	
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/magicpowder.tex"))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/magicpowder.xml"))
	
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/goddess_figure.tex"))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/goddess_figure.xml"))	
	
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/goddess_tori.tex"))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/goddess_tori.xml"))
	
	table.insert(Assets, Asset("IMAGE", "images/map_icons/goddess_gate.tex"))
	table.insert(Assets, Asset("ATLAS", "images/map_icons/goddess_gate.xml"))
	
	table.insert(Assets, Asset("IMAGE", "images/map_icons/peach.tex"))
	table.insert(Assets, Asset("ATLAS", "images/map_icons/peach.xml"))
	
	table.insert(Assets, Asset("ANIM", "anim/goddess_bird_build.zip"))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/goddess_bird.xml"))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/goddess_bird.tex"))
	
	table.insert(Assets, Asset("ANIM", "anim/goddess_feather.zip"))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/goddess_feather.xml"))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/goddess_feather.tex"))
	
	table.insert(Assets, Asset("ANIM", "anim/goddess_deer_build.zip"))
	
	table.insert(Assets, Asset("ANIM", "anim/goddess_lantern.zip"))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/goddess_lantern.xml"))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/goddess_lantern.tex"))
	
	table.insert(Assets, Asset("ANIM", "anim/goddess_fountain_gem.zip"))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/goddess_fountainette.xml"))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/goddess_fountainette.tex"))
	
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/goddess_rabbit_live.xml"))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/goddess_rabbit_live.tex"))
	
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/less_bottle_green_milk.tex"))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/less_bottle_green_milk.xml"))
	
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/peach_juice_bottle_green_less.tex"))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/peach_juice_bottle_green_less.xml"))
	
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/peach_juice_bottle_green_most.tex"))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/peach_juice_bottle_green_most.xml"))
	
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/peach_juice_bottle_green_most.tex"))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/peach_juice_bottle_green_most.xml"))

	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/peach_juice_bottle_green_half.tex"))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/peach_juice_bottle_green_half.xml"))
	
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/peach_juice_bottle_green_less.tex"))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/peach_juice_bottle_green_less.xml"))
	
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/bottle_green_youghurt.tex"))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/bottle_green_youghurt.xml"))
	
RegisterInventoryItemAtlas("images/inventoryimages/peach.xml", "peach.tex")
RegisterInventoryItemAtlas("images/inventoryimages/grilled_peach.xml", "grilled_peach.tex")
RegisterInventoryItemAtlas("images/inventoryimages/magicpowder.xml", "magicpowder.tex")
RegisterInventoryItemAtlas("images/inventoryimages/peachy_meatloaf.xml", "peachy_meatloaf.tex")
RegisterInventoryItemAtlas("images/inventoryimages/peach_juice_bottle_green.xml", "peach_juice_bottle_green.tex")
RegisterInventoryItemAtlas("images/inventoryimages/full_bottle_green.xml", "full_bottle_green.tex")
RegisterInventoryItemAtlas("images/inventoryimages/less_bottle_green_milk.xml", "less_bottle_green_milk.tex")
RegisterInventoryItemAtlas("images/inventoryimages/half_bottle_green_milk.xml", "half_bottle_green_milk.tex")
RegisterInventoryItemAtlas("images/inventoryimages/full_bottle_green_milk.xml", "full_bottle_green_milk.tex")


AddIngredientValues({"full_bottle_green_milk"}, 	{dairy = 3})	
AddIngredientValues({"half_bottle_green_milk"}, 	{dairy = 2})	
AddIngredientValues({"less_bottle_green_milk"}, 	{dairy = 1})	
AddIngredientValues({"full_bottle_green"},			{water = 1})
AddIngredientValues({"magicpowder"},		 {goddessmagic = 1})
AddIngredientValues({"peach"}, 						{fruit = 1})
AddIngredientValues({"grilled_peach"}, 				{fruit = 1})
AddIngredientValues({"peach_juice_bottle_green"},  {peachy = 1})
AddIngredientValues({"peachy_meatloaf"},			 {loaf = 1})

local peach_smoothie =
		{
			name = "peach_smoothie",
			test = function(cooker, names, tags) return (names.peach or names.grilled_peach) and (names.peach or names.grilled_peach)>=2 and tags.dairy and tags.sweetener and not tags.meat and not tags.egg and not tags.inedible and not tags.monster end,
			priority = 100,
			weight = 1,
			foodtype = "VEGGIE",
			health = 10,
			hunger = 65,
			sanity = 10,
			perishtime = TUNING.PERISH_MED,
			cooktime = 0.5,
			cookbook_atlas = "images/inventoryimages/peach_smoothie.xml",				
		}
		
local peach_kabobs =
		{
			name = "peach_kabobs",
			test = function(cooker, names, tags) return (names.peach or names.grilled_peach) and names.twigs and tags.veggie and tags.veggie >=1 and tags.meat and tags.meat >=1 and not tags.egg and not tags.monster  end,
			priority = 100,
			weight = 1,
			foodtype = "MEAT",
			health = 3,
			hunger = 75,
			sanity = 5,
			perishtime = TUNING.PERISH_MED,
			cooktime = 0.5,
			cookbook_atlas = "images/inventoryimages/peach_kabobs.xml",				
		}
		
local peachy_meatloaf =
		{
			name = "peachy_meatloaf",
			test = function(cooker, names, tags) return (names.peach or names.grilled_peach) and (names.peach or names.grilled_peach) >=2 and tags.meat and tags.meat >=2 and not tags.egg end,
			priority = 100,
			weight = 1,
			foodtype = "MEAT",
			health = 10,
			hunger = 80,
			sanity = 5,
			perishtime = TUNING.PERISH_SLOW,
			cooktime = 0.5,
			cookbook_atlas = "images/inventoryimages/peachy_meatloaf.xml",				
		}
		
local caramel_peach =
		{
			name = "caramel_peach",
			test = function(cooker, names, tags) return (names.peach or names.grilled_peach) and tags.sweetener and tags.sweetener >=2 and names.twigs and not tags.meat and not tags.egg and not tags.monster end,
			priority = 100,
			weight = 1,
			foodtype = "VEGGIE",
			health = 40,
			hunger = 45,
			sanity = 5,
			perishtime = TUNING.PERISH_SLOW,
			cooktime = 0.5,
			cookbook_atlas = "images/inventoryimages/caramel_peach.xml",				
		}
		
local peach_juice =
		{
			name = "peach_juice_bottle_green",
			test = function(cooker, names, tags) return (names.peach or names.grilled_peach) and  (names.peach or names.grilled_peach) >=2 and tags.sweetener and tags.water and not tags.meat and not tags.egg and not tags.monster  end,
			priority = 100,
			weight = 1,
			foodtype = "VEGGIE",
			health = 15,
			hunger = 35,
			sanity = 5,
			cooktime = 0.5,
			cookbook_atlas = "images/inventoryimages/peach_juice_bottle_green.xml",				
		}
		
local potion =
		{
			name = "potion_bottle_green",
			test = function(cooker, names, tags) return tags.peachy and tags.goddessmagic and tags.loaf and (names.peach or names.grilled_peach) end,
			priority = 100,
			weight = 1,
			foodtype = "MEAT",
			health = 75,
			hunger = 75,
			sanity = 75,
			cooktime = 0.5,
			cookbook_atlas = "images/inventoryimages/potion_bottle_green.xml",			
		}
		
		
local peach_custard =
		{
			name = "peach_custard",
			test = function(cooker, names, tags) return (names.peach or names.grilled_peach) and tags.egg and tags.sweetener and (names.peach or names.grilled_peach) >=2 and not names.twigs and not tags.meat and not tags.monster end,
			priority = 100,
			weight = 1,
			foodtype = "VEGGIE",
			health = 20,
			hunger = 62,
			sanity = 5,
			perishtime = TUNING.PERISH_SLOW,
			stacksize = 2,
			cooktime = 0.5,
			cookbook_atlas = "images/inventoryimages/peach_custard.xml",			
		}
	
AddCookerRecipe("cookpot",peach_smoothie)
AddCookerRecipe("cookpot",peach_kabobs)
AddCookerRecipe("cookpot",caramel_peach)
AddCookerRecipe("cookpot",peach_custard)
AddCookerRecipe("cookpot",peachy_meatloaf)
AddCookerRecipe("cookpot",potion)
AddCookerRecipe("cookpot",peach_juice)

--local goddess_tab = AddRecipeTab("Wind Goddess Magic", 969, "images/inventoryimages/windyfan1.xml", "windyfan1.tex", nil )
AddRecipe2("magicpowder",			{Ingredient("goddess_butterflywings", 8, "images/inventoryimages/goddess_butterflywings.xml"), Ingredient("nightmarefuel", 8), Ingredient("cutgreengrass", 8, "images/inventoryimages/cutgreengrass.xml")},											TECH.GODDESS_TWO, {atlas="images/inventoryimages/magicpowder.xml", numtogive=8}, 	{"CRAFTING_STATION"})
AddRecipe2("goddess_ribbon",		{Ingredient("goddess_rabbit_fur", 3, "images/inventoryimages/goddess_rabbit_fur.xml"), Ingredient("silk", 9), Ingredient("magicpowder", 1, "images/inventoryimages/magicpowder.xml")},																TECH.GODDESS_TWO, {atlas="images/inventoryimages/goddess_ribbon.xml", numtogive=3}, {"CRAFTING_STATION"})
AddRecipe2("forbidden_fruit",		{Ingredient("mixed_gem", 1, "images/inventoryimages/mixed_gem.xml"), Ingredient("magicpowder", 8, "images/inventoryimages/magicpowder.xml"), Ingredient("peach", 1, "images/inventoryimages/peach.xml")},											TECH.GODDESS_TWO, {atlas="images/inventoryimages/forbidden_fruit.xml"}, 			{"CRAFTING_STATION"})
AddRecipe2("glass_bomb",			{Ingredient("full_bottle_green_dirty", 3, "images/inventoryimages/full_bottle_green_dirty.xml"), Ingredient("magicpowder", 3, "images/inventoryimages/magicpowder.xml"), Ingredient("gunpowder", 9)},												TECH.GODDESS_TWO, {atlas="images/inventoryimages/glass_bomb.xml", numtogive=3}, 	{"CRAFTING_STATION"})
AddRecipe2("goddess_hat",			{Ingredient("eyebrellahat", 1), Ingredient("goddess_feather", 2, "images/inventoryimages/goddess_feather.xml"), Ingredient("forbidden_fruit", 1, "images/inventoryimages/forbidden_fruit.xml")},													TECH.GODDESS_TWO, {atlas="images/inventoryimages/goddess_hat.xml"}, 				{"CRAFTING_STATION"})
AddRecipe2("goddess_bowtie",		{Ingredient("goddess_ribbon", 8, "images/inventoryimages/goddess_ribbon.xml"), Ingredient("goddess_butterfly", 3, "images/inventoryimages/goddess_butterfly.xml"), Ingredient("forbidden_fruit", 1, "images/inventoryimages/forbidden_fruit.xml")},	TECH.GODDESS_TWO, {atlas="images/inventoryimages/goddess_bowtie.xml"}, 				{"CRAFTING_STATION"})
AddRecipe2("usedfan",				{Ingredient("goddess_feather", 8, "images/inventoryimages/goddess_feather.xml"), Ingredient("goldnugget", 12), Ingredient("goose_feather", 8)},																										TECH.GODDESS_TWO, {atlas="images/inventoryimages/usedfan.xml"}, 					{"CRAFTING_STATION"})
AddRecipe2("windyfan",				{Ingredient("usedfan", 1, "images/inventoryimages/usedfan.xml"), Ingredient("magicpowder", 8, "images/inventoryimages/magicpowder.xml"), Ingredient("forbidden_fruit", 1, "images/inventoryimages/forbidden_fruit.xml")},							TECH.GODDESS_TWO, {atlas="images/inventoryimages/windyfan.xml"}, 					{"CRAFTING_STATION"})
AddRecipe2("goddess_sword",			{Ingredient("magicpowder", 10, "images/inventoryimages/magicpowder.xml"), Ingredient("nightsword", 3), Ingredient("forbidden_fruit", 1, "images/inventoryimages/forbidden_fruit.xml")},																TECH.GODDESS_TWO, {atlas="images/inventoryimages/goddess_sword.xml"}, 				{"CRAFTING_STATION"})
AddRecipe2("goddess_flute",			{Ingredient("staff_tornado", 3), Ingredient("cutreeds", 10), Ingredient("forbidden_fruit", 1, "images/inventoryimages/forbidden_fruit.xml")},																										TECH.GODDESS_TWO, {atlas="images/inventoryimages/goddess_flute.xml"}, 				{"CRAFTING_STATION"})
AddRecipe2("goddess_bell",			{Ingredient("goldnugget", 8), Ingredient("steelwool", 8), Ingredient("forbidden_fruit", 1, "images/inventoryimages/forbidden_fruit.xml")},																											TECH.GODDESS_TWO, {atlas="images/inventoryimages/goddess_bell.xml"}, 				{"CRAFTING_STATION"})
AddRecipe2("goddess_staff",			{Ingredient("goldnugget", 8), Ingredient("goddess_ribbon", 8, "images/inventoryimages/goddess_ribbon.xml"), Ingredient("forbidden_fruit", 1, "images/inventoryimages/forbidden_fruit.xml")},														TECH.GODDESS_TWO, {atlas="images/inventoryimages/goddess_staff.xml"}, 				{"CRAFTING_STATION"})
AddRecipe2("goddess_lantern",		{Ingredient("yellowstaff", 2), Ingredient("goddess_ribbon", 8, "images/inventoryimages/goddess_ribbon.xml"), Ingredient("forbidden_fruit", 1, "images/inventoryimages/forbidden_fruit.xml")},														TECH.GODDESS_TWO, {atlas="images/inventoryimages/goddess_lantern.xml"}, 			{"CRAFTING_STATION"})
AddRecipe2("goddess_fountainette",	{Ingredient("full_bottle_green", 4, "images/inventoryimages/full_bottle_green.xml"), Ingredient("moonrocknugget", 15), Ingredient("forbidden_fruit", 1, "images/inventoryimages/forbidden_fruit.xml")},												TECH.GODDESS_TWO, {atlas="images/inventoryimages/goddess_fountainette.xml"}, 		{"CRAFTING_STATION"})
AddRecipe2("goddess_figure",		{Ingredient("windyfan", 1, "images/inventoryimages/windyfan.xml"), Ingredient("marble", 15), Ingredient("forbidden_fruit", 1, "images/inventoryimages/forbidden_fruit.xml")},																		TECH.GODDESS_TWO, {atlas="images/inventoryimages/goddess_figure.xml"}, 				{"CRAFTING_STATION"})
AddRecipe2("gem_seeds",				{Ingredient("mixed_gem", 1, "images/inventoryimages/mixed_gem.xml"), Ingredient("seeds", 3), Ingredient("peach_pit", 3, "images/inventoryimages/peach_pit.xml")},																					TECH.GODDESS_TWO, {atlas="images/inventoryimages/gem_seeds.xml", numtogive=3}, 		{"CRAFTING_STATION"})
AddRecipe2("peachy_poop",			{Ingredient("guano", 6), Ingredient("peach", 6, "images/inventoryimages/peach.xml")},																																								TECH.SCIENCE_TWO, {atlas="images/inventoryimages/peachy_poop.xml", numtogive=6},	{"GARDENING"})


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	
-- New Action (Extrafillable)
	
AddAction("FILLED", "Fill", function(act)
	return	act.target ~= nil
        and act.invobject ~= nil
        and act.invobject.components.extrafillable ~= nil
        and act.target:HasTag("goddess_fountain")
        and act.invobject.components.extrafillable:Fill()
end)	

AddStategraphActionHandler("wilson", ActionHandler(GLOBAL.ACTIONS.FILLED, "dolongaction"))
AddStategraphActionHandler("wilson_client", ActionHandler(GLOBAL.ACTIONS.FILLED, "dolongaction"))

AddComponentAction("USEITEM", "extrafillable", function(inst, doer, target, actions)
	if target:HasTag("goddess_fountain") then
		table.insert(actions, ACTIONS.FILLED)
	end
end)

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	
-- New Action (Milker)

AddAction("MILK", "Milk", function(act)
	return	act.target ~= nil
        and act.invobject ~= nil
        and act.invobject.components.milker ~= nil
        and act.target:HasTag("goddess_deer")
		and act.target:HasTag("windy4")
		and act.target:HasTag("milkable")
        and act.invobject.components.milker:Fill()
end)

AddStategraphActionHandler("wilson", ActionHandler(GLOBAL.ACTIONS.MILK, "dolongaction"))
AddStategraphActionHandler("wilson_client", ActionHandler(GLOBAL.ACTIONS.MILK, "dolongaction"))

AddComponentAction("USEITEM", "milker", function(inst, doer, target, actions)
	if target:HasTag("goddess_deer") and target:HasTag("windy4") then
		table.insert(actions, ACTIONS.MILK)
	end
end)

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Mod Action (give) [too lazy to make new component for goddess item repair]

AddStategraphActionHandler("wilson", ActionHandler(GLOBAL.ACTIONS.GIVE, function(inst, action)
	return action.invobject ~= nil and action.target ~= nil and (
	(action.target:HasTag("moonportal") and action.invobject:HasTag("moonportalkey") and "dochannelaction") or
    (action.invobject.prefab == "quagmire_portal_key" and action.target:HasTag("quagmire_altar") and "quagmireportalkey") or
	(action.target:HasTag("goddess_item") and "dolongaction") or
    (action.target:HasTag("trader") and "give"))
		or "give"
end))

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Mod Action (play_goddess_bell) [this should make the bell appear in cave servers and let this bell work with other bell mods]

AddStategraphState("wilson", State {
	name = "play_goddess_bell",
	tags = {"doing", "playing"},

	onenter = function(inst)
		inst.components.locomotor:Stop()
		inst.AnimState:PlayAnimation("action_uniqueitem_pre")
		inst.AnimState:PushAnimation("bell", false)
		inst.AnimState:OverrideSymbol("bell01", "goddess_bell", "bell01")
		inst.AnimState:Show("ARM_normal")
		inst.components.inventory:ReturnActiveActionItem(inst.bufferedaction ~= nil and inst.bufferedaction.invobject or nil)
	end,

	timeline = {
		TimeEvent(15 * FRAMES, function(inst)
			inst.SoundEmitter:PlaySound("dontstarve_DLC001/common/glommer_bell")
		end),
		TimeEvent(60 * FRAMES, function(inst)
			inst:PerformBufferedAction()
		end),
	},

	events = {
		EventHandler("animover", function(inst)
			if inst.AnimState:AnimDone() then
				inst.sg:GoToState("idle")
			end
		end),
	},

	onexit = function(inst)
		if inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) then
			inst.AnimState:Show("ARM_carry") 
			inst.AnimState:Hide("ARM_normal")
		end
	end,
})

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Mod Action (play_goddess_flute)

AddStategraphState("wilson", State {
	name = "play_goddess_flute",
	tags = { "doing", "playing" },

	onenter = function(inst)
		inst.components.locomotor:Stop()
		inst.AnimState:PlayAnimation("action_uniqueitem_pre")
		inst.AnimState:PushAnimation("flute", false)
		inst.AnimState:OverrideSymbol("pan_flute01", "goddess_flute", "pan_flute01")
		inst.AnimState:Hide("ARM_carry") 
		inst.AnimState:Show("ARM_normal")
		inst.components.inventory:ReturnActiveActionItem(inst.bufferedaction ~= nil and inst.bufferedaction.invobject or nil)
	end,

	timeline =
	{
		TimeEvent(30*FRAMES, function(inst)
			inst.SoundEmitter:PlaySound("dontstarve/wilson/flute_LP", "flute")
			inst:PerformBufferedAction()
		end),
		TimeEvent(85*FRAMES, function(inst)
			inst.SoundEmitter:KillSound("flute")
		end),
	},

	events =
	{
		EventHandler("animqueueover", function(inst)
			if inst.AnimState:AnimDone() then
				inst.sg:GoToState("idle")
			end
		end),
	},

	onexit = function(inst)
		inst.SoundEmitter:KillSound("flute")
		if inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) then
			inst.AnimState:Show("ARM_carry")
			inst.AnimState:Hide("ARM_normal")
		end
	end,
})

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Minimap Icons

AddMinimapAtlas("images/map_icons/goddess_statue.xml")
AddMinimapAtlas("images/map_icons/goddess_statue2.xml")
AddMinimapAtlas("images/map_icons/goddess_statue3.xml")
AddMinimapAtlas("images/map_icons/goddess_fountain.xml")
AddMinimapAtlas("images/map_icons/goddess_shrine.xml")
AddMinimapAtlas("images/map_icons/goddess_gate.xml")
AddMinimapAtlas("images/map_icons/peach.xml")

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Peach Farming

local Crop = require('components/crop')
local oldCropHarvest = Crop.Harvest
	
Crop.Harvest = function(self, harvester, ...)
	if self.product_prefab == "peach" then
	    if self.matured then
			local product = GLOBAL.SpawnPrefab("peach")
	        if harvester then
	        	local rnd = math.random() * 100
	        	local count = 0
	        	if rnd <= 15 then count = 1 elseif rnd <= 75 then count = 2 else count = 3 end
	        	product.components.stackable:SetStackSize(count)
	            harvester.components.inventory:GiveItem(product)
	        else
	            product.Transform:SetPosition(self.grower.Transform:GetWorldPosition())
	            Launch(product, self.grower, TUNING.LAUNCH_SPEED_SMALL)
	        end
	        GLOBAL.ProfileStatsAdd("peach")

	        self.matured = false
	        self.growthpercent = 0
	        self.product_prefab = nil
	        self.grower.components.grower:RemoveCrop(self.inst)
	        self.grower = nil

	        return true
	    end
		
	    return
	end
	
	return oldCropHarvest(self, harvester, ...)
end

	

